import csv
from matplotlib import pyplot as plt
import datetime
import numpy as np
import pandas as pd
import camelot

def MergeTimestamps(datx):
	mergedDat = []
	for ts, level in datx:
		if len(mergedDat) == 0:
			mergedDat.append([(ts, level)])
			continue
		
		prevTs = mergedDat[-1][0][0]
		
		if ts - prevTs < 7*24*60*60:
			mergedDat[-1].append((ts, level))
		
		else:
			mergedDat.append([(ts, level)])

	return mergedDat

def AverageAtTimestamps(mergedDat):
	averageDat = []
	for tsGroup in mergedDat:
		
		tsAvg = sum([a[0] for a in tsGroup]) / len(tsGroup)
		#levelAvg = sum([a[1] for a in tsGroup]) / len(tsGroup)
		levelAvg = np.median([a[1] for a in tsGroup])

		averageDat.append((tsAvg, levelAvg))

	return averageDat	

def ProcessLocation(datByLoc, bias = 1.0):

	datx = []
	for li in datByLoc:
		try:
			dateOn = li['Date On*']
			dateOff = li['Date Off*']
			no2 = float(li['mg/m3 *'])
		except ValueError:
			continue
		if dateOn is pd.NaT or dateOff is pd.NaT: continue

		#print (type(dateOn), type(dateOff), dateOn, dateOff)
		datx.append(((dateOn.timestamp()+dateOff.timestamp())/2.0, no2*bias))

	datx.sort(key=lambda r: r[0])

	mergedDat = MergeTimestamps(datx)

	return AverageAtTimestamps(mergedDat)

def ProcessWithMinReadings(byLoc, bias = 0.84):
	#Bias from 2019 NDDT AST report

	allDatx = []
	for locId in byLoc:
		datx = []
		for li in byLoc[locId]:
			try:
				dateOn = li['Date On*']
				dateOff = li['Date Off*']
				no2 = float(li['mg/m3 *'])
			except ValueError:
				continue
			if dateOn is pd.NaT or dateOff is pd.NaT: continue

			datx.append(((dateOn.timestamp()+dateOff.timestamp())/2.0, no2*bias))

		mergedDat = MergeTimestamps(datx)

		#print (len(mergedDat))
		if len(mergedDat) >= 9:
			allDatx.extend(datx)

	allDatx.sort(key=lambda r: r[0])
	mergedDat = MergeTimestamps(allDatx)
	return AverageAtTimestamps(mergedDat)

def GetPortsmouthMonthly2018():
	print ("Portsmouth monthly 2018")
	tables = camelot.read_pdf('/home/tim/Documents/airqual/portsmouth/2019-air-quality-annual-status-report.pdf', pages='94-98')

	tables2 = [t.df for t in tables]

	result = pd.concat(tables2)
	result.reset_index(drop=True, inplace=True)

	header = result.iloc[:3].to_numpy()
	for c in range(header.shape[1]):
		header[0, c] = header[0, c].replace("\n", "")
		if header[1, c] != "":
			header[0, c] = header[1, c].replace("\n", "")
		if header[2, c] != "":
			header[0, c] = header[2, c].replace("\n", "")

	result.columns = header[0, :]
	result = result.drop([0, 1, 2])

	cols = list(result.columns)
	for i in range(1,13):
		cols[i] = "2018-{0:02d}".format(i)
	result.columns = cols

	for i in range(1,13):
		k = "2018-{0:02d}".format(i)
		result[k] = pd.to_numeric(result[k])

	result.drop('Raw Data', axis=1, inplace=True)
	result.drop('Bias Adjusted (0.891) and Annualised (1)', axis=1, inplace=True)
	result.drop('Distance Corrected to Nearest Exposure (2)', axis=1, inplace=True)

	for i in range(1,13):
		k = "2018-{0:02d}".format(i)
		result.loc[:, k] *= 0.891

	#result.to_csv("portsmouth/monthly2018.csv", index=False)

	return result, 1.0

def GetPortsmouthMonthly2019():
	print ("Portsmouth monthly 2019")
	tables = camelot.read_pdf('/home/tim/Documents/airqual/portsmouth/2020-Air-Quality-Annual-Status-Report_Accessible.pdf', pages='153-158')

	tables2 = [t.df for t in tables]

	result = pd.concat(tables2)
	result.reset_index(drop=True, inplace=True)

	header = result.iloc[:3].to_numpy()
	for c in range(header.shape[1]):
		header[0, c] = header[0, c].replace("\n", "")
		if header[1, c] != "":
			header[0, c] = header[1, c].replace("\n", "")
		if header[2, c] != "":
			header[0, c] = header[2, c].replace("\n", "")

	result.columns = header[0, :]
	result = result.drop([0, 1, 2])

	cols = list(result.columns)
	for i in range(1,13):
		cols[i] = "2019-{0:02d}".format(i)
	result.columns = cols

	for i in range(1,13):
		k = "2019-{0:02d}".format(i)
		result[k] = pd.to_numeric(result[k])

	result.drop('Raw Data', axis=1, inplace=True)
	result.drop('Bias Adjusted (0.84) and Annualised (1)', axis=1, inplace=True)
	result.drop('Distance Corrected to Nearest Exposure (2)', axis=1, inplace=True)

	result.to_csv("portsmouth/testc.csv", index=False)

	for i in range(1,13):
		k = "2019-{0:02d}".format(i)
		result.loc[:, k] *= 0.84

	result.to_csv("portsmouth/monthly2019.csv", index=False)

	return result, 1.00

def GetPortsmouthMonthly2020():
	print ("Portsmouth monthly 2020")
	tables = camelot.read_pdf('/home/tim/Documents/airqual/portsmouth/2021-ASR 2020 Submission to DEFRA 25012022.pdf', pages='153-159')

	tables2 = [t.df for t in tables]

	result = pd.concat(tables2)
	result.reset_index(drop=True, inplace=True)

	header = result.iloc[:3].to_numpy()
	for c in range(header.shape[1]):
		header[0, c] = header[0, c].replace("\n", "")
		if header[1, c] != "":
			header[0, c] = header[1, c].replace("\n", "")
		if header[2, c] != "":
			header[0, c] = header[2, c].replace("\n", "")

	result.columns = header[0, :]
	result = result.drop([0, 1, 2])

	cols = list(result.columns)
	for i in range(1,13):
		cols[i] = "2020-{0:02d}".format(i)
	result.columns = cols

	for i in range(1,13):
		k = "2020-{0:02d}".format(i)
		result[k] = result[k].replace(r'\n','', regex=True) 
		result[k] = pd.to_numeric(result[k])
	
	result.drop('Raw Data', axis=1, inplace=True)
	result.drop('Bias Adjusted (0.822) and Annualised (1)', axis=1, inplace=True)
	result.drop('Distance Corrected to Nearest Exposure (2)', axis=1, inplace=True)

	for i in range(1,13):
		k = "2020-{0:02d}".format(i)
		result.loc[:, k] *= 0.822

	#result.set_index([int(i) for i in result['Site ID']])
	#result.to_csv("portsmouth/monthly2020.csv", index=False)

	return result, 1.0

def GetPortsmouthMonthly2021():
	print ("Portsmouth monthly 2021")
	tables = camelot.read_pdf('/home/tim/Documents/airqual/portsmouth/ASR 2022 Portsmouth City Council.pdf', pages='166-172')

	tables2 = [t.df for t in tables]
	result = pd.concat(tables2)
	#result.reset_index(drop=True, inplace=True)
	if 0:

		header = result.iloc[:3].to_numpy()
		for c in range(header.shape[1]):
			header[0, c] = header[0, c].replace("\n", "")
			if header[1, c] != "":
				header[0, c] = header[1, c].replace("\n", "")
			if header[2, c] != "":
				header[0, c] = header[2, c].replace("\n", "")

		result.columns = header[0, :]
		result = result.drop([0, 1, 2])

	if 0:

		cols = list(result.columns)
		for i in range(1,13):
			cols[i] = "2020-{0:02d}".format(i)
		result.columns = cols

		for i in range(1,13):
			k = "2020-{0:02d}".format(i)
			result[k] = result[k].replace(r'\n','', regex=True) 
			result[k] = pd.to_numeric(result[k])
		
		result.drop('Raw Data', axis=1, inplace=True)
		result.drop('Bias Adjusted (0.822) and Annualised (1)', axis=1, inplace=True)
		result.drop('Distance Corrected to Nearest Exposure (2)', axis=1, inplace=True)

		for i in range(1,13):
			k = "2020-{0:02d}".format(i)
			result.loc[:, k] *= 0.822

		#result.set_index([int(i) for i in result['Site ID']])
		#result.to_csv("portsmouth/monthly2020.csv", index=False)

	return result, 1.0

def GetPortsmouthSiteInfo2020():

	# https://www.whatdotheyknow.com/request/portsmouth_air_quality_data_for_3
	ex = pd.ExcelFile('/home/tim/Documents/airqual/portsmouth/Location code index.xlsx')

	sh = ex.parse(0, header=0, skiprows=1)

	out = {}
	for index, row in sh.iterrows():
		out[row['Unnamed: 0']] = row['Site ID']

	return out

def SortByLocation(sh, column):
	#Sort by location
	byLoc = {}	
	for index, row in sh.iterrows():
		if pd.isnull(row[column]): continue
		locId = int(row[column])

		if locId in byLoc:
			byLoc[locId].append(row)
		else:
			byLoc[locId]=[row]

	return byLoc

def CalcMonthyAverages(byLoc):
	locOrdered = list(byLoc.keys())
	locOrdered.sort()
	startDt = None
	endDt = None
	locValOut = {}

	#Get monthly averages
	for locId in locOrdered:

		averageDat = ProcessLocation(byLoc[locId], 1.0)

		monthBins = {}

		for ts, level in averageDat:
			dt = datetime.datetime.fromtimestamp(ts)
			monthBins["{}-{:02d}".format(dt.year, dt.month)] = level
			if startDt is None or dt < startDt:
				startDt = dt
			if endDt is None or dt > endDt:
				endDt = dt
		
		#print (locId, monthBins)
		locValOut[locId] = monthBins

	return locValOut, startDt, endDt

def WriteMonthlyTable(locValOut, startDt, endDt, outFina):

	locOrdered = list(locValOut.keys())
	locOrdered.sort()

	#Pad out missing data to create nice table
	pr = list(pd.period_range(start=startDt,end=endDt, freq='M'))

	out = csv.writer(open(outFina, "wt"))	
	out.writerow(['loc']+pr)
	for loc in locOrdered:
		monthBins = locValOut[loc]
		
		valsRow = []
		for m in pr:
			k = "{}-{:02d}".format(m.year, m.month)
			if k in monthBins:
				valsRow.append(monthBins[k])
			else:					
				valsRow.append(None)

		out.writerow([loc]+valsRow)

	del out

def WriteMonthlyDataFrame(locValOut, startDt, endDt):

	locOrdered = list(locValOut.keys())
	locOrdered.sort()

	#Pad out missing data to create nice table
	pr = list(pd.period_range(start=startDt,end=endDt, freq='M'))

	out = pd.DataFrame(columns = ['Site ID'] + [str(t) for t in pr])

	for loc in locOrdered:
		monthBins = locValOut[loc]
		
		valsRow = [loc]
		keysRow = ["Site ID"]
		for m in pr:
			k = "{}-{:02d}".format(m.year, m.month)
			keysRow.append(k)
			if k in monthBins:
				valsRow.append(monthBins[k])
			else:					
				valsRow.append(None)

		out = pd.concat([out, pd.DataFrame(data=[dict(zip(keysRow, valsRow))])])

	out.reset_index(drop=True, inplace=True)

	return out

def convert_date(datestr): #31/03/2021
	if isinstance(datestr, datetime.datetime):
		return datestr

	return datetime.datetime.strptime(datestr, '%d/%m/%Y')

def ParseMonthlyRawFoi(fina):

	ef = pd.ExcelFile(fina)
	print (ef.sheet_names)

	siteCodes = GetPortsmouthSiteInfo2020()
	siteCodesLower = dict([(k.lower().replace(" ", ""), int(v)) for (k, v) in siteCodes.items()])

	locations = set()
	collect = []

	for sheet in ef.sheet_names:
		sh = ef.parse(sheet, header=0, skiprows=1, converters={2: convert_date, 3: convert_date})

		sh = sh.drop([0])
		sh['Site ID'] = None

		#Add Site ID based on look up
		for index, row in sh.iterrows():
			loc = str(row['Location']).lower().replace(" ", "")
	
			shims = {"tap": "lr-tap", 'hs121a': 'hs-121a'}
			shims2 = {"mr-4": 18, "vrn-106(col19)": 23, 'kc-84-88(vh)': 43, 'sr-1w': 47, 'mr-sh(fence)': 61, 'mr-col41': 59, 'sr-28e': 48, 'mw-(opp6)': 44}

			if loc in shims:
				loc = shims[loc]
			elif loc in shims2:
				sh.loc[index, 'Site ID'] = shims2[loc]
	
			if loc in siteCodesLower:
				sh.loc[index, 'Site ID'] = siteCodesLower[loc]

		#sh.to_csv("portsmouth/raw{}.csv".format(sheet), index=False)
		collect.append(sh)

	out = pd.concat(collect)
	out.reset_index(drop=True, inplace=True)	
	return out

def GetPortsmouthMonthly2021RawFoi():
	# https://www.whatdotheyknow.com/request/portsmouth_air_quality_data_for_3
	return ParseMonthlyRawFoi("845 2.xlsx")

def GetPortsmouthMonthly2022RawFoi():
	# https://www.whatdotheyknow.com/request/portsmouth_air_quality_data_for_3
	return ParseMonthlyRawFoi("845 3.xlsx")

def ProcessMonthlyRawFoi(df, scale=1.0):

	#df.to_csv("portsmouth/raw2021.csv", index=False)

	print (df.shape)
	print (df.columns)

	byLoc = SortByLocation(df, 'Site ID')

	locValOut, startDt, endDt = CalcMonthyAverages(byLoc)

	for loc in locValOut:
		readingsAtLoc = locValOut[loc]
		for k, v in readingsAtLoc.items():
			readingsAtLoc[k] = v * scale

	return WriteMonthlyDataFrame(locValOut, startDt, endDt)

def MergeMonthlyDataFrames(frames):

	siteIds = set()
	for df in frames:
		siteIds.update(df['Site ID'])

	monthCols = set()
	for df in frames:
		monthCols.update(df.columns[1:])
	monthCols = list(monthCols)
	monthCols.sort()

	me = pd.DataFrame(columns = ['Site ID'] + monthCols)

	for df in frames:
		#print (df.columns)
		for index, row in df.iterrows():

			siteIndex = me[me['Site ID'] == row['Site ID']].index
			if len(siteIndex) == 0:
				me = pd.concat([me, pd.DataFrame(data=[row])])
			else:
				rowDict = dict(row)
				for k, v in rowDict.items():
					me.iloc[siteIndex[0]][k] = v

	me.sort_values("Site ID", inplace=True)
	me.reset_index(drop=True, inplace=True)
	me.to_csv("portsmouth/test.csv", index=False)

	print (me)

if __name__=="__main__":

	if 0:
		df1, scaling1 = GetPortsmouthMonthly2018()
		df2, scaling2 = GetPortsmouthMonthly2019()

		df1.to_csv("monthly2018.csv", index=False)
		df2.to_csv("monthly2019.csv", index=False)

	if 0:
		df3, scaling3 = GetPortsmouthMonthly2020()
		df3.to_csv("monthly2020.csv", index=False)

	if 1:
		df4, scaling4 = GetPortsmouthMonthly2021()
		df4.to_csv("monthly2021.csv", index=False)
		exit(0)

	df4 = ProcessMonthlyRawFoi(GetPortsmouthMonthly2021RawFoi(), scale=0.82)
	df5 = ProcessMonthlyRawFoi(GetPortsmouthMonthly2022RawFoi(), scale=0.82)

	if 1:
		df1 = pd.read_csv("monthly2018.csv", converters={0: np.int32})
		df1['Site ID'] = df1['Site ID'].astype('object')
		df2 = pd.read_csv("monthly2019.csv", converters={0: np.int32})
		df2['Site ID'] = df2['Site ID'].astype('object')
		df3 = pd.read_csv("monthly2020.csv", converters={0: np.int32})
		df3['Site ID'] = df3['Site ID'].astype('object')

	MergeMonthlyDataFrames([df1, df2, df3, df4, df5])


	


