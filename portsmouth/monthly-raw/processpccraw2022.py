import csv
from matplotlib import pyplot as plt
import datetime
import numpy as np
import pandas as pd

def convert_date(datestr): #31/03/2021
	if isinstance(datestr, datetime.datetime):
		return datestr.date()

	return datetime.datetime.strptime(datestr, '%d/%m/%Y').date()

def GetDateRangeOverlap(startA, endA, startB, endB):

	if startA >= startB and endA < endB:
		return endA - startA
	if startB >= startA and endB < endA:
		return endB - startB
	if startA >= startB and startA < endB:
		return endB - startA
	if startB >= startA and startB < endA:
		return endA - startB
	return datetime.timedelta(days=0)

def GetPortsmouthSiteInfo2020():

	# https://www.whatdotheyknow.com/request/portsmouth_air_quality_data_for_3
	ex = pd.ExcelFile('/home/tim/Documents/airqual/portsmouth/Location code index.xlsx')

	sh = ex.parse(0, header=0, skiprows=1)

	out = {}
	for index, row in sh.iterrows():
		k = row['Unnamed: 0'].lower()
		v = row['Site ID']
		if k in out and v != out[k]:
			raise RuntimeError("Inconsistent site ID")
		out[k] = v

	shims = {"tap": 26, 'hs121a': 3, "mr-4": 18, "vrn-106(col19)": 23, 'kc-84-88(vh)': 43, 
		'sr-1w': 47, 'mr-sh(fence)': 61, 'mr-col41': 59, 'sr-28e': 48, 'mw-(opp6)': 44}
	for k, v in shims.items():
		out[k] = v

	return out

   

if __name__ == "__main__":

	locMap = GetPortsmouthSiteInfo2020()

	ex = pd.ExcelFile("/home/tim/Documents/airqual/portsmouth/DT 2022 Raw.xlsx")

	# Group data by site ID
	siteData = {}
	for sheet in ex.sheet_names:

		sh = ex.parse(sheet, header=0, skiprows=1, converters={2: convert_date, 3: convert_date})
		if sh.shape[0] == 0:
			continue
		sh = sh.drop([0])

		for index, row in sh.iterrows():
			loc = row['Location']
			#print (row['Location'], row['Date On*'], row['Date Off*'], row['mg/m3 *'])
			loc1 = str(loc).lower().split("/")[0]
			data = {'loc': loc, 'start': row['Date On*'], 
						'end': row['Date Off*'], 'no2': row['mg/m3 *']}

			if loc1 in locMap:
				locId = locMap[loc1]
				#print (loc1, locMap[loc1])
			else:
				locId = loc1
				#print ("Missing", loc1)

			if str(locId) not in siteData:
				siteData[str(locId)] = [data]
			else:
				siteData[str(locId)].append(data)

	siteIds = list(siteData.keys())
	siteIds.sort()
	averagedData = {}
	for siteId in siteIds:
		#print ("siteId", siteId)		
		data = siteData[siteId]
		averagedData[siteId] = [] 

		for i in range(1,13):
			#Get data for this month
			yearOffset = i // 12
			start = datetime.date(2022, i, 1)
			end = datetime.date(2022+yearOffset, (i % 12) + 1, 1)

			readings = []
			weights = []
			for row in data:
				if pd.isna(row['start']): continue
				#print (start, end, row['start'], row['end'])
				overlap = GetDateRangeOverlap(start, end, row['start'], row['end'])
				if overlap.days == 0: continue

				readings.append(row['no2'])
				weights.append(overlap.days)

			if len(readings) > 0:
				average = np.average(readings, weights=weights)
				#print (siteId, i, average)
				averagedData[siteId].append(average)
			else:
				averagedData[siteId].append(float("nan"))
		
	#Typical seasonal variation from 2018-2020
	seasonal = np.array([30.2, 29.4, 28.5, 28.0, 26.9, 26.1, 25.3, 26.4, 27.1, 28.7, 29.5, 30.4])

	if 1:
		#Add missing data for December
		for siteId in averagedData:
			row = np.array(averagedData[siteId])

			notNanMask = np.where(np.isnan(row) == False)
			nanMask = np.where(np.isnan(row))
			numVals = np.sum(np.isnan(row) == False)
			print (siteId, notNanMask, numVals)

			if numVals >= 9:
				avSeasonal = np.average(seasonal[notNanMask])
				avRow = np.average(row[notNanMask])
				#if np.isnan(row[11]):
				#	av = np.average(row[:11])

				for i in nanMask:
					row[i] = seasonal[i] * avRow / avSeasonal

				averagedData[siteId] = row

	#Apply bias correction (from 2022 Air Quality Annual Status Report (ASR), Table C.2)
	biasCorrection = np.average([0.845, #2021
		0.822, #2020
		0.84, #2019
		])

	print ("biasCorrection", biasCorrection)
	biasCorrectedData = {}
	for siteId in averagedData:
		biasCorrectedData[siteId] = np.array(averagedData[siteId]) * biasCorrection

	out = csv.writer(open("monthly2022provisional.csv", "wt"))
	for siteId in biasCorrectedData:
		out.writerow([siteId] + list(biasCorrectedData[siteId]))
	del out

