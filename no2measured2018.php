<!DOCTYPE html>
<html>
<head>
	
	<title>Portsmouth NO2 measured levels 2018</title>

	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
    <link rel="stylesheet" href="leaflet/leaflet.css"/>
    <script src="leaflet/leaflet.js"></script>
	
</head>
<body>

<div id="mapid" style="width: 100%; height: 500px;"></div>

<script>

	var mymap3 = L.map('mapid').setView([50.80, -1.083333], 13);

	/*L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoidGltc2MiLCJhIjoiY2l0cmdpeXBoMDAwcDJ1cGtvdXNhN2FxbiJ9.9RHQRB-C3ZPZhRNRs5TYFA', {
		maxZoom: 18,
		attribution: 'Map data &copy; <a href="http://www.openstreetmap.org/about/">OpenStreetMap</a> contributors, ' +
			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		id: 'mapbox.streets'
	}).addTo(mymap3);*/
	
	L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoidGltc2MiLCJhIjoiY2l0cmdpeXBoMDAwcDJ1cGtvdXNhN2FxbiJ9.9RHQRB-C3ZPZhRNRs5TYFA', {
    attribution: '© <a href="https://www.mapbox.com/about/maps/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> <strong><a href="https://www.mapbox.com/map-feedback/" target="_blank">Improve this map</a></strong>',
    tileSize: 512,
    maxZoom: 18,
    zoomOffset: -1,
    id: 'mapbox/streets-v11',
    accessToken: 'pk.eyJ1IjoidGltc2MiLCJhIjoiY2l0cmdpeXBoMDAwcDJ1cGtvdXNhN2FxbiJ9.9RHQRB-C3ZPZhRNRs5TYFA'
    }).addTo(mymap3);

	var caution0sIcon = L.icon({
		iconUrl: 'caution-0s.png',

		iconSize:     [13, 13], // size of the icon
		iconAnchor:   [6, 6], // point of the icon which will correspond to marker's location
		popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
	});

	var caution10sIcon = L.icon({
		iconUrl: 'caution-10s.png',

		iconSize:     [26, 26], // size of the icon
		iconAnchor:   [13, 13], // point of the icon which will correspond to marker's location
		popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
	});

	var caution20sIcon = L.icon({
		iconUrl: 'caution-20s.png',

		iconSize:     [38, 38], // size of the icon
		iconAnchor:   [19, 19], // point of the icon which will correspond to marker's location
		popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
	});

	var caution30sIcon = L.icon({
		iconUrl: 'caution-30s.png',

		iconSize:     [51, 51], // size of the icon
		iconAnchor:   [25, 25], // point of the icon which will correspond to marker's location
		popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
	});

	var caution40sIcon = L.icon({
		iconUrl: 'caution-40s.png',

		iconSize:     [64, 64], // size of the icon
		iconAnchor:   [32, 32], // point of the icon which will correspond to marker's location
		popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
	});

	var caution50sIcon = L.icon({
		iconUrl: 'caution-50s.png',

		iconSize:     [77, 77], // size of the icon
		iconAnchor:   [38, 38], // point of the icon which will correspond to marker's location
		popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
	});


	L.marker([50.7949094204, -1.09511006592], {icon: caution40sIcon}).addTo(mymap3).bindPopup("<b>Receptor 1</b><br/>NO2 2012: 42.54<br/>NO2 2013: 41.9<br/>NO2 2014: 42.57<br/>NO2 2015: 44.33<br/>NO2 2016: 43.52<br/>NO2 2017: 38.8<br/>NO2 2018: 42.92<br/>");
	L.marker([50.7904050607, -1.09756636788], {icon: caution10sIcon}).addTo(mymap3).bindPopup("<b>Receptor 2</b><br/>NO2 2012: 17.48<br/>NO2 2013: 16.5<br/>NO2 2014: 16.55<br/>NO2 2015: 15.74<br/>NO2 2016: 17.4<br/>NO2 2017: 16.38<br/>NO2 2018: 17.09<br/>");
	L.marker([50.7912378406, -1.10176415235], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 3</b><br/>NO2 2012: 26.63<br/>NO2 2013: 22.1<br/>NO2 2014: 25.67<br/>NO2 2015: 24.07<br/>NO2 2016: 25.75<br/>NO2 2017: 23.7<br/>NO2 2018: 24.13<br/>");
	L.marker([50.7996237383, -1.10469691451], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 4</b><br/>NO2 2012: 36.35<br/>NO2 2013: 31.51<br/>NO2 2014: 27.97<br/>NO2 2015: 30.54<br/>NO2 2016: 34.7<br/>NO2 2017: 34.2<br/>NO2 2018: 34.04<br/>");
	L.marker([50.8157302542, -1.08962586277], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 5</b><br/>NO2 2012: 28.62<br/>NO2 2013: 27.49<br/>NO2 2014: 28.93<br/>NO2 2015: 27.53<br/>NO2 2016: 29.52<br/>NO2 2017: 24.48<br/>NO2 2018: 28.08<br/>");
	L.marker([50.8157460256, -1.08819182271], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 6</b><br/>NO2 2012: 35.62<br/>NO2 2013: 38.29<br/>NO2 2014: 34.85<br/>NO2 2015: 46.06<br/>NO2 2016: 36.08<br/>NO2 2017: 32.08<br/>NO2 2018: 30.86<br/>");
	L.marker([50.8164877662, -1.08874520288], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 7</b><br/>NO2 2012: 29.78<br/>NO2 2013: 30<br/>NO2 2014: 26.53<br/>NO2 2015: 26.05<br/>NO2 2016: 28.09<br/>NO2 2017: 27.32<br/>NO2 2018: 27.74<br/>");
	L.marker([50.8348836655, -1.05431886385], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 8</b><br/>NO2 2012: 28.81<br/>NO2 2013: 27.22<br/>NO2 2014: 28.37<br/>NO2 2015: 28.43<br/>NO2 2016: 29.94<br/>NO2 2017: 26.75<br/>NO2 2018: 25.97<br/>");
	L.marker([50.8455525863, -1.06928707865], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 9</b><br/>NO2 2012: 35.07<br/>NO2 2013: 31.95<br/>NO2 2014: 33.88<br/>NO2 2015: 34.98<br/>NO2 2016: 40.86<br/>NO2 2017: 37.06<br/>NO2 2018: 36.7<br/>");
	L.marker([50.8392868493, -1.04830819438], {icon: caution10sIcon}).addTo(mymap3).bindPopup("<b>Receptor 10</b><br/>NO2 2012: 17.91<br/>NO2 2013: 17.66<br/>NO2 2014: 16.66<br/>NO2 2015: 16.48<br/>NO2 2016: 19.54<br/>NO2 2017: 17.58<br/>NO2 2018: 17.17<br/>");
	L.marker([50.8267881031, -1.0519403069], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 11</b><br/>NO2 2012: 31.76<br/>NO2 2013: 29.54<br/>NO2 2014: 33.29<br/>NO2 2015: 28.27<br/>NO2 2016: 28.1<br/>NO2 2017: 23.5<br/>NO2 2018: 22.9<br/>");
	L.marker([50.829384078, -1.0626796576], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 14</b><br/>NO2 2012: 22.68<br/>NO2 2013: 21.61<br/>NO2 2014: 27.21<br/>NO2 2015: 26.87<br/>NO2 2016: 22.2<br/>NO2 2017: 21.28<br/>NO2 2018: 21.66<br/>");
	L.marker([50.8076952372, -1.06295735503], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 15</b><br/>NO2 2012: 28.82<br/>NO2 2013: 28.15<br/>NO2 2014: 27.57<br/>NO2 2015: 26.21<br/>NO2 2016: 28.97<br/>NO2 2017: 28.95<br/>NO2 2018: 27.64<br/>");
	L.marker([50.8336738617, -1.07161127882], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 16</b><br/>NO2 2012: 36.44<br/>NO2 2013: 33.98<br/>NO2 2014: 32.32<br/>NO2 2015: 32.01<br/>NO2 2016: 36.45<br/>NO2 2017: 35.44<br/>NO2 2018: 29.59<br/>");
	L.marker([50.8077697909, -1.06328230212], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 18</b><br/>NO2 2012: 29.52<br/>NO2 2013: 27.8<br/>NO2 2014: 28.9<br/>NO2 2015: 26.91<br/>NO2 2016: 29.3<br/>NO2 2017: 29.62<br/>NO2 2018: 26.01<br/>");
	L.marker([50.797791469, -1.05929577867], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 19</b><br/>NO2 2012: 34.52<br/>NO2 2013: 30.1<br/>NO2 2014: 37.24<br/>NO2 2015: 35.08<br/>NO2 2016: 39.61<br/>NO2 2017: 34.72<br/>NO2 2018: 37.68<br/>");
	L.marker([50.7904626949, -1.05490282405], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 20</b><br/>NO2 2012: 26.07<br/>NO2 2013: 27.42<br/>NO2 2014: 28.9<br/>NO2 2015: 27.58<br/>NO2 2016: 29.12<br/>NO2 2017: 29.73<br/>NO2 2018: 28.42<br/>");
	L.marker([50.7865784278, -1.07630350536], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 21</b><br/>NO2 2012: 35.79<br/>NO2 2013: 32.88<br/>NO2 2014: 35.18<br/>NO2 2015: 35.28<br/>NO2 2016: 40.05<br/>NO2 2017: 38.37<br/>NO2 2018: 36.5<br/>");
	L.marker([50.7897018007, -1.08235676161], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 22</b><br/>NO2 2012: 31.61<br/>NO2 2013: 28.69<br/>NO2 2014: 30.8<br/>NO2 2015: 28.06<br/>NO2 2016: 31.23<br/>NO2 2017: 26.48<br/>NO2 2018: 29.28<br/>");
	L.marker([50.7938159555, -1.07949522118], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 23</b><br/>NO2 2012: 41.05<br/>NO2 2013: 30.4<br/>NO2 2014: 28.8<br/>NO2 2015: 31<br/>NO2 2016: 37<br/>NO2 2017: 34<br/>NO2 2018: 34.6<br/>");
	L.marker([50.8025313179, -1.07737976203], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 24</b><br/>NO2 2012: 39.12<br/>NO2 2013: 42.48<br/>NO2 2014: 40.49<br/>NO2 2015: 36.32<br/>NO2 2016: 37.74<br/>NO2 2017: 38.3<br/>NO2 2018: 36.76<br/>");
	L.marker([50.8098229091, -1.07830063386], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 25</b><br/>NO2 2012: 44.58<br/>NO2 2013: 38.69<br/>NO2 2014: 52.18<br/>NO2 2015: 41.79<br/>NO2 2016: 43.65<br/>NO2 2017: 44.28<br/>NO2 2018: 38.21<br/>");
	L.marker([50.8136955193, -1.08015490116], {icon: caution40sIcon}).addTo(mymap3).bindPopup("<b>Receptor 26</b><br/>NO2 2012: 50.48<br/>NO2 2013: 50.93<br/>NO2 2014: 40.81<br/>NO2 2015: 43.12<br/>NO2 2016: 49.16<br/>NO2 2017: 43.09<br/>NO2 2018: 46.02<br/>");
	L.marker([50.8090759634, -1.08623523033], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 30</b><br/>NO2 2012: 37.97<br/>NO2 2013: 38.83<br/>NO2 2014: 44.12<br/>NO2 2015: 34.31<br/>NO2 2016: 39.34<br/>NO2 2017: 38.48<br/>NO2 2018: 39.17<br/>");
	L.marker([50.8047779511, -1.08516951716], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 32</b><br/>NO2 2012: 35.99<br/>NO2 2013: 31.09<br/>NO2 2014: 34.93<br/>NO2 2015: 31.68<br/>NO2 2016: 33.51<br/>NO2 2017: 32.87<br/>NO2 2018: 31.87<br/>");
	L.marker([50.8040105819, -1.08708620794], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 34</b><br/>NO2 2012: 38.8<br/>NO2 2013: 34.65<br/>NO2 2014: 35.52<br/>NO2 2015: 34.65<br/>NO2 2016: 36.06<br/>NO2 2017: 36.17<br/>NO2 2018: 33.34<br/>");
	L.marker([50.7938792542, -1.09562656471], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 35</b><br/>NO2 2012: 31.1<br/>NO2 2013: 28.96<br/>NO2 2014: 41.42<br/>NO2 2015: 28.48<br/>NO2 2016: 30.68<br/>NO2 2017: 30.13<br/>NO2 2018: 30.08<br/>");
	L.marker([50.7899394631, -1.08628206396], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 36</b><br/>NO2 2012: 32.84<br/>NO2 2013: 30.33<br/>NO2 2014: 34.81<br/>NO2 2015: 29<br/>NO2 2016: 33.32<br/>NO2 2017: 29.74<br/>NO2 2018: 31.47<br/>");
	L.marker([50.8134106656, -1.08510024423], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 42</b><br/>NO2 2018: 38.05<br/>");
	L.marker([50.8132240751, -1.081952684], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 43</b><br/>NO2 2018: 32.5<br/>");
	L.marker([50.8034809674, -1.08835958712], {icon: caution40sIcon}).addTo(mymap3).bindPopup("<b>Receptor 44</b><br/>NO2 2018: 40.41<br/>");
	L.marker([50.8032552979, -1.08825044601], {icon: caution40sIcon}).addTo(mymap3).bindPopup("<b>Receptor 45</b><br/>NO2 2018: 41.97<br/>");
	L.marker([50.8074369339, -1.08824001413], {icon: caution40sIcon}).addTo(mymap3).bindPopup("<b>Receptor 46</b><br/>NO2 2018: 44.51<br/>");
	L.marker([50.8150702868, -1.08458519649], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 47</b><br/>NO2 2018: 36.77<br/>");
	L.marker([50.8150151119, -1.08443012834], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 48</b><br/>NO2 2018: 30.54<br/>");
	L.marker([50.7989654844, -1.10680965471], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 49</b><br/>NO2 2018: 34.64<br/>");
	L.marker([50.7996740596, -1.1018862342], {icon: caution40sIcon}).addTo(mymap3).bindPopup("<b>Receptor 50</b><br/>NO2 2018: 40.37<br/>");
	L.marker([50.7996530987, -1.10266711312], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 51</b><br/>NO2 2018: 33.18<br/>");
	L.marker([50.7998166449, -1.10405465386], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 52</b><br/>NO2 2018: 32.29<br/>");
	L.marker([50.7983572335, -1.0955685139], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 54</b><br/>NO2 2017: 33.82<br/>NO2 2018: 36.28<br/>");
	L.marker([50.7924268151, -1.10435201151], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 55</b><br/>NO2 2017: 30.4<br/>NO2 2018: 25.38<br/>");
	L.marker([50.7941491558, -1.1037941066], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 56</b><br/>NO2 2017: 36.17<br/>NO2 2018: 35.09<br/>");
	L.marker([50.7903462865, -1.10043341035], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 57</b><br/>NO2 2018: 20.32<br/>");
	L.marker([50.7930185093, -1.10060913192], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 58</b><br/>NO2 2017: 33.8<br/>NO2 2018: 29.32<br/>");
	L.marker([50.7987773055, -1.06110652905], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 59</b><br/>NO2 2018: 38.23<br/>");
	L.marker([50.8000791535, -1.06196026072], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 60</b><br/>NO2 2018: 29.77<br/>");
	L.marker([50.801273447, -1.06285876392], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 61</b><br/>NO2 2018: 33.67<br/>");
	L.marker([50.8009374609, -1.06245394828], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 62</b><br/>NO2 2018: 22.04<br/>");
	L.marker([50.7973102785, -1.05984465251], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 63</b><br/>NO2 2018: 34.17<br/>");
	L.marker([50.7972505382, -1.06024316556], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 64</b><br/>NO2 2018: 37.88<br/>");
	L.marker([50.7990800676, -1.05516890792], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 65</b><br/>NO2 2017: 27.62<br/>NO2 2018: 28.24<br/>");
	L.marker([50.7977158498, -1.06107103933], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 66</b><br/>NO2 2018: 31.9<br/>");
	L.marker([50.7980267927, -1.05836871594], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 67</b><br/>NO2 2018: 36.73<br/>");
	L.marker([50.7982375501, -1.05774011594], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 68</b><br/>NO2 2018: 36.86<br/>");
	L.marker([50.7979888263, -1.05923505619], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 69</b><br/>NO2 2018: 31.14<br/>");
	L.marker([50.7916457364, -1.05551743126], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 70</b><br/>NO2 2017: 23.69<br/>NO2 2018: 25.14<br/>");
	L.marker([50.8464054824, -1.06799167023], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 71</b><br/>NO2 2018: 27.78<br/>");
	L.marker([50.8459890478, -1.06876701507], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 72</b><br/>NO2 2018: 26.49<br/>");
	L.marker([50.8456928165, -1.06882975909], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 73</b><br/>NO2 2018: 27.4<br/>");
	L.marker([50.8442502212, -1.06946933579], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 74</b><br/>NO2 2018: 37.27<br/>");
	L.marker([50.8463710567, -1.06931333558], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 75</b><br/>NO2 2018: 25.71<br/>");
	L.marker([50.814263484, -1.06450093011], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 76</b><br/>NO2 2018: 31.25<br/>");
	L.marker([50.8146584126, -1.06440786982], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 77</b><br/>NO2 2018: 21.23<br/>");
	L.marker([50.7921388127, -1.05755060251], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 78</b><br/>NO2 2018: 25.04<br/>");
	L.marker([50.7921261526, -1.05709683313], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 79</b><br/>NO2 2018: 39.32<br/>");
	L.marker([50.7867048725, -1.07637194658], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 80</b><br/>NO2 2018: 38.35<br/>");
	L.marker([50.7866066399, -1.07532407655], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 81</b><br/>NO2 2018: 35.22<br/>");
	L.marker([50.7864110766, -1.0767465848], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 82</b><br/>NO2 2018: 30.79<br/>");
	L.marker([50.7867451058, -1.07691024476], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 83</b><br/>NO2 2018: 32.43<br/>");
	L.marker([50.786867396, -1.07645386392], {icon: caution40sIcon}).addTo(mymap3).bindPopup("<b>Receptor 84</b><br/>NO2 2018: 42.82<br/>");
	L.marker([50.7866210275, -1.07713967333], {icon: caution40sIcon}).addTo(mymap3).bindPopup("<b>Receptor 85</b><br/>NO2 2018: 40.41<br/>");
	L.marker([50.7935027577, -1.0762805499], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 86</b><br/>NO2 2018: 28.89<br/>");
	L.marker([50.795033331, -1.07650578271], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 87</b><br/>NO2 2018: 27.3<br/>");
	L.marker([50.7868687436, -1.07662407708], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 88</b><br/>NO2 2018: 35.35<br/>");
	L.marker([50.7864187209, -1.07657619617], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 89</b><br/>NO2 2018: 30.85<br/>");
	L.marker([50.8031033941, -1.06340401094], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 90</b><br/>NO2 2018: 23.98<br/>");
	L.marker([50.8031601911, -1.06375766503], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 91</b><br/>NO2 2018: 26.69<br/>");
	L.marker([50.7933704045, -1.05749745671], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 92</b><br/>NO2 2017: 28.69<br/>NO2 2018: 27.27<br/>");
	L.marker([50.7914407788, -1.08164168712], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 93</b><br/>NO2 2018: 35.04<br/>");
	L.marker([50.7965912153, -1.07677306019], {icon: caution40sIcon}).addTo(mymap3).bindPopup("<b>Receptor 94</b><br/>NO2 2018: 40.33<br/>");
	L.marker([50.7959497749, -1.0775377361], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 95</b><br/>NO2 2018: 29.31<br/>");
	L.marker([50.7863068332, -1.07267713436], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 96</b><br/>NO2 2018: 23.47<br/>");
	L.marker([50.7944852141, -1.06639987608], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 97</b><br/>NO2 2018: 25.84<br/>");
	L.marker([50.801038038, -1.05485979298], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 98</b><br/>NO2 2018: 22.51<br/>");
	L.marker([50.8008640937, -1.0544801479], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 99</b><br/>NO2 2018: 23.57<br/>");
	L.marker([50.8466435375, -1.03855581825], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 100</b><br/>NO2 2018: 22.14<br/>");
	L.marker([50.846743967, -1.03983215994], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 101</b><br/>NO2 2018: 28.17<br/>");
	L.marker([50.8473410667, -1.08396719428], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 102</b><br/>NO2 2018: 28.72<br/>");
	L.marker([50.8315334661, -1.07048934007], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 103</b><br/>NO2 2018: 24.73<br/>");
	L.marker([50.8176639413, -1.07935298875], {icon: caution40sIcon}).addTo(mymap3).bindPopup("<b>Receptor 108</b><br/>NO2 2018: 44.18<br/>");
	L.marker([50.8173481239, -1.07921723984], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 109</b><br/>NO2 2018: 35.76<br/>");
	L.marker([50.8176771878, -1.07989217527], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 110</b><br/>NO2 2018: 27.72<br/>");
	L.marker([50.8176339175, -1.08010596499], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 111</b><br/>NO2 2018: 28.73<br/>");
	L.marker([50.8006068263, -1.09458849478], {icon: caution50sIcon}).addTo(mymap3).bindPopup("<b>Receptor 117</b><br/>NO2 2018: 50.42<br/>");
	L.marker([50.8006013181, -1.09387906191], {icon: caution50sIcon}).addTo(mymap3).bindPopup("<b>Receptor 118</b><br/>NO2 2018: 50.38<br/>");
	L.marker([50.8027430446, -1.09175150966], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 119</b><br/>NO2 2018: 31.97<br/>");
	L.marker([50.8028972284, -1.09191881818], {icon: caution40sIcon}).addTo(mymap3).bindPopup("<b>Receptor 120</b><br/>NO2 2018: 47.51<br/>");
	L.marker([50.8145463292, -1.07971235177], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 121</b><br/>NO2 2018: 37.32<br/>");
	L.marker([50.8147185042, -1.0798793089], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 122</b><br/>NO2 2018: 37.68<br/>");
	L.marker([50.8551149037, -1.11356450259], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 124</b><br/>NO2 2018: 28.56<br/>");
	L.marker([50.8374424324, -1.06940626382], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 125</b><br/>NO2 2018: 39.58<br/>");
	L.marker([50.8432877311, -1.09582178042], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 126</b><br/>NO2 2018: 37.53<br/>");
	L.marker([50.8000981692, -1.08015228222], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 134</b><br/>NO2 2018: 25.41<br/>");
	L.marker([50.8150683846, -1.07977306754], {icon: caution40sIcon}).addTo(mymap3).bindPopup("<b>Receptor C2</b><br/>NO2 2012: 43.9<br/>NO2 2013: 39.68<br/>NO2 2014: 45.68<br/>NO2 2015: 38.4<br/>NO2 2016: 41.21<br/>NO2 2017: 44.6<br/>NO2 2018: 40.57<br/>");
	L.marker([50.8314069933, -1.07266447423], {icon: caution10sIcon}).addTo(mymap3).bindPopup("<b>Receptor C4</b><br/>NO2 2012: 21.1<br/>NO2 2013: 20.27<br/>NO2 2014: 22.17<br/>NO2 2015: 18.78<br/>NO2 2016: 20.05<br/>NO2 2017: 19.41<br/>NO2 2018: 18.68<br/>");
	L.marker([50.8169156542, -1.06441957858], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor C6</b><br/>NO2 2012: 36.1<br/>NO2 2013: 33.52<br/>NO2 2014: 35.93<br/>NO2 2015: 32.81<br/>NO2 2016: 34.34<br/>NO2 2017: 35.22<br/>NO2 2018: 34<br/>");
	L.marker([50.8074035236, -1.08741747904], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor C7</b><br/>NO2 2012: 36.9<br/>NO2 2013: 35.94<br/>NO2 2014: 36.51<br/>NO2 2015: 30.25<br/>NO2 2016: 35.48<br/>NO2 2017: 33.54<br/>NO2 2018: 33.95<br/>");
	L.marker([50.7983752168, -1.09556816676], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor C8</b><br/>NO2 2018: 30.52<br/>");

</script>



</body>
</html>

