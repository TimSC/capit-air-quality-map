from ostn02python import OSGB
from ostn02python import OSTN02
import csv

def Sites():

	sites = csv.DictReader(open("havant/sites.csv", "rt"))
	outDict = {}

	for li in sites:
		try:
			siteId = li['Site ID']
			code, xin = li['X OS Grid Ref'].split(" ")
			#print (li['X OS Grid Ref'].split(" "), li['Y OS Grid Ref'])
			xin, yin = float(xin), float(li['Y OS Grid Ref'])
		
			#print (xin, yin)
			xin, yin = OSGB.parse_grid(code[1:3], xin, yin)
			(x,y,h) = OSTN02.OSGB36_to_ETRS89 (xin, yin)
			(gla, glo) = OSGB.grid_to_ll(x, y)

			#print (xin, yin, gla, glo)

		except ValueError:
			pass
		outDict[siteId] = ((gla, glo))

	return outDict

def Measurements():
	
	data = csv.DictReader(open("havant/no2.csv", "rt"))
	rowsDict = {}
	
	for li in data:
		#print (li)
		siteId = li['Site ID']

		years = []
		measurements = []
		for y in range(2014, 2019):
			reading = li[str(y)]
			if reading == "": continue
			years.append(y)
			measurements.append(float(reading))

		rowsDict[siteId] = ((years, measurements))

	return rowsDict

if __name__=="__main__":


	print (Measurements())
