<!DOCTYPE html>
<html>
<head>
	
	<title>Portsmouth Road Scheme Graphs</title>

	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
    <link rel="stylesheet" href="leaflet/leaflet.css"/>
    <script src="leaflet/leaflet.js"></script>
	
</head>
<body>

<div id="mapid3" style="width: 100%; height: 500px;"></div>

<script>

	var mymap3 = L.map('mapid3').setView([50.816667, -1.083333], 13);

/*	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoidGltc2MiLCJhIjoiY2l0cmdpeXBoMDAwcDJ1cGtvdXNhN2FxbiJ9.9RHQRB-C3ZPZhRNRs5TYFA', {
		maxZoom: 18,
		attribution: 'Map data &copy; <a href="http://www.openstreetmap.org/about/">OpenStreetMap</a> contributors, ' +
			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		id: 'mapbox.streets'
	}).addTo(mymap3);*/
	
	L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoidGltc2MiLCJhIjoiY2l0cmdpeXBoMDAwcDJ1cGtvdXNhN2FxbiJ9.9RHQRB-C3ZPZhRNRs5TYFA', {
    attribution: '© <a href="https://www.mapbox.com/about/maps/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> <strong><a href="https://www.mapbox.com/map-feedback/" target="_blank">Improve this map</a></strong>',
    tileSize: 512,
    maxZoom: 18,
    zoomOffset: -1,
    id: 'mapbox/streets-v11',
    accessToken: 'pk.eyJ1IjoidGltc2MiLCJhIjoiY2l0cmdpeXBoMDAwcDJ1cGtvdXNhN2FxbiJ9.9RHQRB-C3ZPZhRNRs5TYFA'
    }).addTo(mymap3);

	var caution0sIcon = L.icon({
		iconUrl: 'caution-0s.png',

		iconSize:     [13, 13], // size of the icon
		iconAnchor:   [6, 6], // point of the icon which will correspond to marker's location
		popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
	});

	var caution10sIcon = L.icon({
		iconUrl: 'caution-10s.png',

		iconSize:     [26, 26], // size of the icon
		iconAnchor:   [13, 13], // point of the icon which will correspond to marker's location
		popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
	});

	var caution20sIcon = L.icon({
		iconUrl: 'caution-20s.png',

		iconSize:     [38, 38], // size of the icon
		iconAnchor:   [19, 19], // point of the icon which will correspond to marker's location
		popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
	});

	var caution30sIcon = L.icon({
		iconUrl: 'caution-30s.png',

		iconSize:     [51, 51], // size of the icon
		iconAnchor:   [25, 25], // point of the icon which will correspond to marker's location
		popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
	});

	var caution40sIcon = L.icon({
		iconUrl: 'caution-40s.png',

		iconSize:     [64, 64], // size of the icon
		iconAnchor:   [32, 32], // point of the icon which will correspond to marker's location
		popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
	});

	var caution50sIcon = L.icon({
		iconUrl: 'caution-50s.png',

		iconSize:     [77, 77], // size of the icon
		iconAnchor:   [38, 38], // point of the icon which will correspond to marker's location
		popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
	});


	L.marker([50.7949094204, -1.09511006592], {icon: caution40sIcon}).addTo(mymap3).bindPopup("<b>Receptor 1</b><br/>NO2 2012: 42.54<br/>NO2 2013: 41.9<br/>NO2 2014: 42.57<br/>NO2 2015: 44.33<br/>NO2 2016: 43.52<br/>");
	L.marker([50.7904050607, -1.09756636788], {icon: caution10sIcon}).addTo(mymap3).bindPopup("<b>Receptor 2</b><br/>NO2 2012: 17.48<br/>NO2 2013: 16.5<br/>NO2 2014: 16.55<br/>NO2 2015: 15.74<br/>NO2 2016: 17.4<br/>");
	L.marker([50.7912378406, -1.10176415235], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 3</b><br/>NO2 2012: 26.63<br/>NO2 2013: 22.1<br/>NO2 2014: 25.67<br/>NO2 2015: 24.07<br/>NO2 2016: 25.75<br/>");
	L.marker([50.7996237383, -1.10469691451], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 4</b><br/>NO2 2012: 36.35<br/>NO2 2013: 31.51<br/>NO2 2014: 27.97<br/>NO2 2015: 30.54<br/>NO2 2016: 34.7<br/>");
	L.marker([50.8157302542, -1.08962586277], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 5</b><br/>NO2 2012: 28.62<br/>NO2 2013: 27.49<br/>NO2 2014: 28.93<br/>NO2 2015: 27.53<br/>NO2 2016: 29.52<br/>");
	L.marker([50.8157460256, -1.08819182271], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 6</b><br/>NO2 2012: 35.62<br/>NO2 2013: 38.29<br/>NO2 2014: 34.85<br/>NO2 2015: 46.06<br/>NO2 2016: 36.08<br/>");
	L.marker([50.8164877662, -1.08874520288], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 7</b><br/>NO2 2012: 29.78<br/>NO2 2013: 30<br/>NO2 2014: 26.53<br/>NO2 2015: 26.05<br/>NO2 2016: 28.09<br/>");
	L.marker([50.8348836655, -1.05431886385], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 8</b><br/>NO2 2012: 28.81<br/>NO2 2013: 27.22<br/>NO2 2014: 28.37<br/>NO2 2015: 28.43<br/>NO2 2016: 29.94<br/>");
	L.marker([50.8455525863, -1.06928707865], {icon: caution40sIcon}).addTo(mymap3).bindPopup("<b>Receptor 9</b><br/>NO2 2012: 35.07<br/>NO2 2013: 31.95<br/>NO2 2014: 33.88<br/>NO2 2015: 34.98<br/>NO2 2016: 40.86<br/>");
	L.marker([50.8392868493, -1.04830819438], {icon: caution10sIcon}).addTo(mymap3).bindPopup("<b>Receptor 10</b><br/>NO2 2012: 17.91<br/>NO2 2013: 17.66<br/>NO2 2014: 16.66<br/>NO2 2015: 16.48<br/>NO2 2016: 19.54<br/>");
	L.marker([50.8267881031, -1.0519403069], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 11</b><br/>NO2 2012: 31.76<br/>NO2 2013: 29.54<br/>NO2 2014: 33.29<br/>NO2 2015: 28.27<br/>NO2 2016: 28.1<br/>");
	L.marker([50.829384078, -1.0626796576], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 14</b><br/>NO2 2012: 22.68<br/>NO2 2013: 21.61<br/>NO2 2014: 27.21<br/>NO2 2015: 26.87<br/>NO2 2016: 22.2<br/>");
	L.marker([50.8076952372, -1.06295735503], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 15</b><br/>NO2 2012: 28.82<br/>NO2 2013: 28.15<br/>NO2 2014: 27.57<br/>NO2 2015: 26.21<br/>NO2 2016: 28.97<br/>");
	L.marker([50.8336738617, -1.07161127882], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 16</b><br/>NO2 2012: 36.44<br/>NO2 2013: 33.98<br/>NO2 2014: 32.32<br/>NO2 2015: 32.01<br/>NO2 2016: 36.45<br/>");
	L.marker([50.8077697909, -1.06328230212], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 18</b><br/>NO2 2012: 29.52<br/>NO2 2013: 27.8<br/>NO2 2014: 28.9<br/>NO2 2015: 26.91<br/>NO2 2016: 29.3<br/>");
	L.marker([50.797791469, -1.05929577867], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 19</b><br/>NO2 2012: 34.52<br/>NO2 2013: 30.1<br/>NO2 2014: 37.24<br/>NO2 2015: 35.08<br/>NO2 2016: 39.61<br/>");
	L.marker([50.7904626949, -1.05490282405], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 20</b><br/>NO2 2012: 26.07<br/>NO2 2013: 27.42<br/>NO2 2014: 28.9<br/>NO2 2015: 27.58<br/>NO2 2016: 29.12<br/>");
	L.marker([50.7865784278, -1.07630350536], {icon: caution40sIcon}).addTo(mymap3).bindPopup("<b>Receptor 21</b><br/>NO2 2012: 35.79<br/>NO2 2013: 32.88<br/>NO2 2014: 35.18<br/>NO2 2015: 35.28<br/>NO2 2016: 40.05<br/>");
	L.marker([50.7897018007, -1.08235676161], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 22</b><br/>NO2 2012: 31.61<br/>NO2 2013: 28.69<br/>NO2 2014: 30.8<br/>NO2 2015: 28.06<br/>NO2 2016: 31.23<br/>");
	L.marker([50.7938159555, -1.07949522118], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 23</b><br/>NO2 2012: 41.05<br/>NO2 2013: 30.4<br/>NO2 2014: 28.8<br/>NO2 2015: 31<br/>NO2 2016: 37<br/>");
	L.marker([50.8025313179, -1.07737976203], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 24</b><br/>NO2 2012: 39.12<br/>NO2 2013: 42.48<br/>NO2 2014: 40.49<br/>NO2 2015: 36.32<br/>NO2 2016: 37.74<br/>");
	L.marker([50.8098229091, -1.07830063386], {icon: caution40sIcon}).addTo(mymap3).bindPopup("<b>Receptor 25</b><br/>NO2 2012: 44.58<br/>NO2 2013: 38.69<br/>NO2 2014: 52.18<br/>NO2 2015: 41.79<br/>NO2 2016: 43.65<br/>");
	L.marker([50.8136955193, -1.08015490116], {icon: caution40sIcon}).addTo(mymap3).bindPopup("<b>Receptor 26</b><br/>NO2 2012: 50.48<br/>NO2 2013: 50.93<br/>NO2 2014: 40.81<br/>NO2 2015: 43.12<br/>NO2 2016: 49.16<br/>");
	L.marker([50.8090759634, -1.08623523033], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 30</b><br/>NO2 2012: 37.97<br/>NO2 2013: 38.83<br/>NO2 2014: 44.12<br/>NO2 2015: 34.31<br/>NO2 2016: 39.34<br/>");
	L.marker([50.8047779511, -1.08516951716], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 32</b><br/>NO2 2012: 35.99<br/>NO2 2013: 31.09<br/>NO2 2014: 34.93<br/>NO2 2015: 31.68<br/>NO2 2016: 33.51<br/>");
	L.marker([50.8040105819, -1.08708620794], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 34</b><br/>NO2 2012: 38.8<br/>NO2 2013: 34.65<br/>NO2 2014: 35.52<br/>NO2 2015: 34.65<br/>NO2 2016: 36.06<br/>");
	L.marker([50.7938792542, -1.09562656471], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 35</b><br/>NO2 2012: 31.1<br/>NO2 2013: 28.96<br/>NO2 2014: 41.42<br/>NO2 2015: 28.48<br/>NO2 2016: 30.68<br/>");
	L.marker([50.7899394631, -1.08628206396], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 36</b><br/>NO2 2012: 32.84<br/>NO2 2013: 30.33<br/>NO2 2014: 34.81<br/>NO2 2015: 29<br/>NO2 2016: 33.32<br/>");
	L.marker([50.8150683846, -1.07977306754], {icon: caution40sIcon}).addTo(mymap3).bindPopup("<b>Receptor C2</b><br/>NO2 2012: 43.9<br/>NO2 2013: 39.68<br/>NO2 2014: 45.68<br/>NO2 2015: 38.4<br/>NO2 2016: 41.21<br/>");
	L.marker([50.8314069933, -1.07266447423], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor C4</b><br/>NO2 2012: 21.1<br/>NO2 2013: 20.27<br/>NO2 2014: 22.17<br/>NO2 2015: 18.78<br/>NO2 2016: 20.05<br/>");
	L.marker([50.8169156542, -1.06441957858], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor C6</b><br/>NO2 2012: 36.1<br/>NO2 2013: 33.52<br/>NO2 2014: 35.93<br/>NO2 2015: 32.81<br/>NO2 2016: 34.34<br/>");
	L.marker([50.8074035236, -1.08741747904], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor C7</b><br/>NO2 2012: 36.9<br/>NO2 2013: 35.94<br/>NO2 2014: 36.51<br/>NO2 2015: 30.25<br/>NO2 2016: 35.48<br/>");


</script>



</body>
</html>

