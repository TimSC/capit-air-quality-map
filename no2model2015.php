<!DOCTYPE html>
<html>
<head>
	
	<title>Portsmouth Road Scheme Graphs</title>

	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
    <link rel="stylesheet" href="leaflet/leaflet.css"/>
    <script src="leaflet/leaflet.js"></script>
	
</head>
<body>

<div id="mapid2" style="width: 100%; height: 500px;"></div>

<script>

	var mymap2 = L.map('mapid2').setView([50.816667, -1.083333], 13);

	/*L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoidGltc2MiLCJhIjoiY2l0cmdpeXBoMDAwcDJ1cGtvdXNhN2FxbiJ9.9RHQRB-C3ZPZhRNRs5TYFA', {
		maxZoom: 18,
		attribution: 'Map data &copy; <a href="http://www.openstreetmap.org/about/">OpenStreetMap</a> contributors, ' +
			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		id: 'mapbox.streets'
	}).addTo(mymap2);*/
	
	L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoidGltc2MiLCJhIjoiY2l0cmdpeXBoMDAwcDJ1cGtvdXNhN2FxbiJ9.9RHQRB-C3ZPZhRNRs5TYFA', {
    attribution: '© <a href="https://www.mapbox.com/about/maps/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> <strong><a href="https://www.mapbox.com/map-feedback/" target="_blank">Improve this map</a></strong>',
    tileSize: 512,
    maxZoom: 18,
    zoomOffset: -1,
    id: 'mapbox/streets-v11',
    accessToken: 'pk.eyJ1IjoidGltc2MiLCJhIjoiY2l0cmdpeXBoMDAwcDJ1cGtvdXNhN2FxbiJ9.9RHQRB-C3ZPZhRNRs5TYFA'
    }).addTo(mymap2);

	var caution0sIcon = L.icon({
		iconUrl: 'caution-0s.png',

		iconSize:     [13, 13], // size of the icon
		iconAnchor:   [6, 6], // point of the icon which will correspond to marker's location
		popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
	});

	var caution10sIcon = L.icon({
		iconUrl: 'caution-10s.png',

		iconSize:     [26, 26], // size of the icon
		iconAnchor:   [13, 13], // point of the icon which will correspond to marker's location
		popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
	});

	var caution20sIcon = L.icon({
		iconUrl: 'caution-20s.png',

		iconSize:     [38, 38], // size of the icon
		iconAnchor:   [19, 19], // point of the icon which will correspond to marker's location
		popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
	});

	var caution30sIcon = L.icon({
		iconUrl: 'caution-30s.png',

		iconSize:     [51, 51], // size of the icon
		iconAnchor:   [25, 25], // point of the icon which will correspond to marker's location
		popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
	});

	var caution40sIcon = L.icon({
		iconUrl: 'caution-40s.png',

		iconSize:     [64, 64], // size of the icon
		iconAnchor:   [32, 32], // point of the icon which will correspond to marker's location
		popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
	});

	var caution50sIcon = L.icon({
		iconUrl: 'caution-50s.png',

		iconSize:     [77, 77], // size of the icon
		iconAnchor:   [38, 38], // point of the icon which will correspond to marker's location
		popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
	});



	L.marker([50.7827750309, -1.08846188664], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 1</b><br/>Baseline NO2 2015: 20.4<br/>With scheme NO2 2026: 20.3<br/>Without scheme NO2 2026: 20<br/>Impact: Negligible ");
	L.marker([50.783358403, -1.09032870412], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 2</b><br/>Baseline NO2 2015: 20.2<br/>With scheme NO2 2026: 20.3<br/>Without scheme NO2 2026: 19.8<br/>Impact: Negligible ");
	L.marker([50.7853119679, -1.0927319634], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 3</b><br/>Baseline NO2 2015: 20.3<br/>With scheme NO2 2026: 20.2<br/>Without scheme NO2 2026: 19.8<br/>Impact: Negligible ");
	L.marker([50.7855018935, -1.09400869489], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 4</b><br/>Baseline NO2 2015: 25.4<br/>With scheme NO2 2026: 24.4<br/>Without scheme NO2 2026: 23.3<br/>Impact: Negligible ");
	L.marker([50.7837857523, -1.08545781255], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 5</b><br/>Baseline NO2 2015: 27.5<br/>With scheme NO2 2026: 26.9<br/>Without scheme NO2 2026: 25.8<br/>Impact: Negligible ");
	L.marker([50.7824562082, -1.08164907575], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 6</b><br/>Baseline NO2 2015: 29.0<br/>With scheme NO2 2026: 27.7<br/>Without scheme NO2 2026: 26.9<br/>Impact: Negligible ");
	L.marker([50.7867092594, -1.06242300153], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 7</b><br/>Baseline NO2 2015: 32.2<br/>With scheme NO2 2026: 29.4<br/>Without scheme NO2 2026: 28.2<br/>Impact: Negligible ");
	L.marker([50.7860241848, -1.06873155713], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 8</b><br/>Baseline NO2 2015: 21.3<br/>With scheme NO2 2026: 19.5<br/>Without scheme NO2 2026: 19<br/>Impact: Negligible ");
	L.marker([50.7884456412, -1.0827970612], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 9</b><br/>Baseline NO2 2015: 25.0<br/>With scheme NO2 2026: 23.6<br/>Without scheme NO2 2026: 23<br/>Impact: Negligible ");
	L.marker([50.7846675718, -1.08386994481], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 10</b><br/>Baseline NO2 2015: 26.2<br/>With scheme NO2 2026: 25.6<br/>Without scheme NO2 2026: 25.4<br/>Impact: Negligible ");
	L.marker([50.7876995476, -1.09334350705], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 11</b><br/>Baseline NO2 2015: 24.2<br/>With scheme NO2 2026: 24<br/>Without scheme NO2 2026: 24.4<br/>Impact: Negligible ");
	L.marker([50.7885948585, -1.09715224385], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 12</b><br/>Baseline NO2 2015: 23.0<br/>With scheme NO2 2026: 22.9<br/>Without scheme NO2 2026: 22.4<br/>Impact: Negligible ");
	L.marker([50.7900191815, -1.10301018834], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 13</b><br/>Baseline NO2 2015: 30.3<br/>With scheme NO2 2026: 29.7<br/>Without scheme NO2 2026: 28<br/>Impact: Negligible ");
	L.marker([50.7906024631, -1.10301018834], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 14</b><br/>Baseline NO2 2015: 28.3<br/>With scheme NO2 2026: 27.7<br/>Without scheme NO2 2026: 27.6<br/>Impact: Negligible ");
	L.marker([50.7919114291, -1.10051036954], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 15</b><br/>Baseline NO2 2015: 30.5<br/>With scheme NO2 2026: 29.1<br/>Without scheme NO2 2026: 28.7<br/>Impact: Negligible ");
	L.marker([50.7915112828, -1.10113264203], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 16</b><br/>Baseline NO2 2015: 30.1<br/>With scheme NO2 2026: 28.8<br/>Without scheme NO2 2026: 27.1<br/>Impact: Negligible ");
	L.marker([50.792874486, -1.09989883661], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 17</b><br/>Baseline NO2 2015: 30.1<br/>With scheme NO2 2026: 28.7<br/>Without scheme NO2 2026: 27.2<br/>Impact: Negligible ");
	L.marker([50.7942037337, -1.10243084192], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 18</b><br/>Baseline NO2 2015: 37.6<br/>With scheme NO2 2026: 33.7<br/>Without scheme NO2 2026: 32.6<br/>Impact: Slight Adverse");
	L.marker([50.7961297195, -1.10343935251], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 19</b><br/>Baseline NO2 2015: 33.9<br/>With scheme NO2 2026: 31.1<br/>Without scheme NO2 2026: 30<br/>Impact: Slight Adverse");
	L.marker([50.7967536134, -1.10449077845], {icon: caution40sIcon}).addTo(mymap2).bindPopup("<b>Receptor 20</b><br/>Baseline NO2 2015: 45.8<br/>With scheme NO2 2026: 40<br/>Without scheme NO2 2026: 39.3<br/>Impact: Moderate Adverse");
	L.marker([50.7996017187, -1.10543491602], {icon: caution40sIcon}).addTo(mymap2).bindPopup("<b>Receptor 21</b><br/>Baseline NO2 2015: 43.6<br/>With scheme NO2 2026: 38.9<br/>Without scheme NO2 2026: 37.6<br/>Impact: Moderate Adverse");
	L.marker([50.7997373385, -1.10455515146], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 22</b><br/>Baseline NO2 2015: 39.5<br/>With scheme NO2 2026: 35.8<br/>Without scheme NO2 2026: 32.6<br/>Impact: Moderate Adverse");
	L.marker([50.7996695286, -1.10191585779], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 23</b><br/>Baseline NO2 2015: 37.5<br/>With scheme NO2 2026: 34.3<br/>Without scheme NO2 2026: 31.4<br/>Impact: Moderate Adverse");
	L.marker([50.7999000816, -1.09985592127], {icon: caution40sIcon}).addTo(mymap2).bindPopup("<b>Receptor 24</b><br/>Baseline NO2 2015: 45.5<br/>With scheme NO2 2026: 38.4<br/>Without scheme NO2 2026: 36.2<br/>Impact: Moderate Adverse");
	L.marker([50.7997915862, -1.09865429163], {icon: caution40sIcon}).addTo(mymap2).bindPopup("<b>Receptor 25</b><br/>Baseline NO2 2015: 40.9<br/>With scheme NO2 2026: 35.4<br/>Without scheme NO2 2026: 33.4<br/>Impact: Slight Adverse");
	L.marker([50.7945699484, -1.09586479425], {icon: caution40sIcon}).addTo(mymap2).bindPopup("<b>Receptor 26</b><br/>Baseline NO2 2015: 46.8<br/>With scheme NO2 2026: 39.3<br/>Without scheme NO2 2026: 37<br/>Impact: Moderate Adverse");
	L.marker([50.7925896422, -1.09597208261], {icon: caution40sIcon}).addTo(mymap2).bindPopup("<b>Receptor 27</b><br/>Baseline NO2 2015: 40.1<br/>With scheme NO2 2026: 40.8<br/>Without scheme NO2 2026: 36.4<br/>Impact: Substantial Adverse");
	L.marker([50.7940002799, -1.09558584452], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 28</b><br/>Baseline NO2 2015: 30.7<br/>With scheme NO2 2026: 29.6<br/>Without scheme NO2 2026: 28.3<br/>Impact: Negligible ");
	L.marker([50.7935933695, -1.09131576777], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 29</b><br/>Baseline NO2 2015: 36.4<br/>With scheme NO2 2026: 33.9<br/>Without scheme NO2 2026: 32.8<br/>Impact: Slight Adverse");
	L.marker([50.7928337941, -1.09118702173], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 30</b><br/>Baseline NO2 2015: 24.3<br/>With scheme NO2 2026: 25.4<br/>Without scheme NO2 2026: 24.1<br/>Impact: Negligible ");
	L.marker([50.7923047968, -1.09161617517], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 31</b><br/>Baseline NO2 2015: 25.2<br/>With scheme NO2 2026: 27.3<br/>Without scheme NO2 2026: 25.5<br/>Impact: Negligible ");
	L.marker([50.7913281706, -1.09193804026], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 32</b><br/>Baseline NO2 2015: 23.3<br/>With scheme NO2 2026: 23.9<br/>Without scheme NO2 2026: 23<br/>Impact: Negligible ");
	L.marker([50.7954651277, -1.08820440531], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 33</b><br/>Baseline NO2 2015: 25.3<br/>With scheme NO2 2026: 26.8<br/>Without scheme NO2 2026: 25.2<br/>Impact: Negligible ");
	L.marker([50.7947327095, -1.08262541056], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 34</b><br/>Baseline NO2 2015: 32.8<br/>With scheme NO2 2026: 29.8<br/>Without scheme NO2 2026: 29.4<br/>Impact: Negligible ");
	L.marker([50.7951531733, -1.08101608515], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 35</b><br/>Baseline NO2 2015: 30.8<br/>With scheme NO2 2026: 28.7<br/>Without scheme NO2 2026: 28.5<br/>Impact: Negligible ");
	L.marker([50.7954380013, -1.07771160364], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 36</b><br/>Baseline NO2 2015: 28.6<br/>With scheme NO2 2026: 27.8<br/>Without scheme NO2 2026: 26.7<br/>Impact: Negligible ");
	L.marker([50.7926845903, -1.08069422007], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 37</b><br/>Baseline NO2 2015: 36.5<br/>With scheme NO2 2026: 33.7<br/>Without scheme NO2 2026: 32.4<br/>Impact: Slight Adverse");
	L.marker([50.7917757935, -1.0812092042], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 38</b><br/>Baseline NO2 2015: 31.2<br/>With scheme NO2 2026: 32.6<br/>Without scheme NO2 2026: 29<br/>Impact: Moderate Adverse");
	L.marker([50.7905007352, -1.0825181222], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 39</b><br/>Baseline NO2 2015: 27.7<br/>With scheme NO2 2026: 27.2<br/>Without scheme NO2 2026: 26.2<br/>Impact: Negligible ");
	L.marker([50.7894765874, -1.08246446729], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 40</b><br/>Baseline NO2 2015: 26.5<br/>With scheme NO2 2026: 26<br/>Without scheme NO2 2026: 25.2<br/>Impact: Negligible ");
	L.marker([50.7889814647, -1.08155251622], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 41</b><br/>Baseline NO2 2015: 34.7<br/>With scheme NO2 2026: 33.5<br/>Without scheme NO2 2026: 32.1<br/>Impact: Slight Adverse");
	L.marker([50.788947552, -1.0799110043], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 42</b><br/>Baseline NO2 2015: 25.1<br/>With scheme NO2 2026: 24.8<br/>Without scheme NO2 2026: 24.7<br/>Impact: Negligible ");
	L.marker([50.7895986716, -1.0788381207], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 43</b><br/>Baseline NO2 2015: 23.6<br/>With scheme NO2 2026: 23.4<br/>Without scheme NO2 2026: 23.5<br/>Impact: Negligible ");
	L.marker([50.7901209172, -1.07800127149], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 44</b><br/>Baseline NO2 2015: 22.3<br/>With scheme NO2 2026: 22.1<br/>Without scheme NO2 2026: 22.1<br/>Impact: Negligible ");
	L.marker([50.7953294888, -1.07226134419], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 45</b><br/>Baseline NO2 2015: 24.5<br/>With scheme NO2 2026: 23<br/>Without scheme NO2 2026: 23.8<br/>Impact: Negligible ");
	L.marker([50.7944749973, -1.06665016294], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 46</b><br/>Baseline NO2 2015: 26.9<br/>With scheme NO2 2026: 26.6<br/>Without scheme NO2 2026: 25.2<br/>Impact: Negligible ");
	L.marker([50.791694401, -1.05612517476], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 47</b><br/>Baseline NO2 2015: 27.6<br/>With scheme NO2 2026: 26.7<br/>Without scheme NO2 2026: 25.8<br/>Impact: Negligible ");
	L.marker([50.7908805367, -1.05492354512], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 48</b><br/>Baseline NO2 2015: 23.8<br/>With scheme NO2 2026: 22.8<br/>Without scheme NO2 2026: 22<br/>Impact: Negligible ");
	L.marker([50.7973028976, -1.05989099622], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 49</b><br/>Baseline NO2 2015: 28.3<br/>With scheme NO2 2026: 27.1<br/>Without scheme NO2 2026: 25.6<br/>Impact: Negligible ");
	L.marker([50.7977369007, -1.05957985997], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 50</b><br/>Baseline NO2 2015: 35.6<br/>With scheme NO2 2026: 36.9<br/>Without scheme NO2 2026: 33.3<br/>Impact: Moderate Adverse");
	L.marker([50.7997712366, -1.05307818532], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 51</b><br/>Baseline NO2 2015: 33.9<br/>With scheme NO2 2026: 35.6<br/>Without scheme NO2 2026: 32.1<br/>Impact: Moderate Adverse");
	L.marker([50.8014257644, -1.05467678189], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 52</b><br/>Baseline NO2 2015: 21.0<br/>With scheme NO2 2026: 20.7<br/>Without scheme NO2 2026: 20.2<br/>Impact: Negligible ");
	L.marker([50.8047075234, -1.04969860196], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 53</b><br/>Baseline NO2 2015: 25.1<br/>With scheme NO2 2026: 24.9<br/>Without scheme NO2 2026: 24.3<br/>Impact: Negligible ");
	L.marker([50.8046092062, -1.06432200015], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 54</b><br/>Baseline NO2 2015: 21.3<br/>With scheme NO2 2026: 21<br/>Without scheme NO2 2026: 20.5<br/>Impact: Negligible ");
	L.marker([50.8073822543, -1.06331348956], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 55</b><br/>Baseline NO2 2015: 25.4<br/>With scheme NO2 2026: 24.9<br/>Without scheme NO2 2026: 24.1<br/>Impact: Negligible ");
	L.marker([50.8094466885, -1.0638928628], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 56</b><br/>Baseline NO2 2015: 28.0<br/>With scheme NO2 2026: 27<br/>Without scheme NO2 2026: 26.3<br/>Impact: Negligible ");
	L.marker([50.8097178704, -1.06462242365], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 57</b><br/>Baseline NO2 2015: 29.5<br/>With scheme NO2 2026: 27.7<br/>Without scheme NO2 2026: 27<br/>Impact: Negligible ");
	L.marker([50.8092026234, -1.06584551096], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 58</b><br/>Baseline NO2 2015: 24.7<br/>With scheme NO2 2026: 22.7<br/>Without scheme NO2 2026: 22.9<br/>Impact: Negligible ");
	L.marker([50.8095416023, -1.06730463266], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 59</b><br/>Baseline NO2 2015: 27.1<br/>With scheme NO2 2026: 25.4<br/>Without scheme NO2 2026: 25.5<br/>Impact: Negligible ");
	L.marker([50.8093517744, -1.07022287607], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 60</b><br/>Baseline NO2 2015: 25.3<br/>With scheme NO2 2026: 24.1<br/>Without scheme NO2 2026: 24.2<br/>Impact: Negligible ");
	L.marker([50.8085856686, -1.07377411008], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 61</b><br/>Baseline NO2 2015: 28.1<br/>With scheme NO2 2026: 25.8<br/>Without scheme NO2 2026: 26.4<br/>Impact: Negligible ");
	L.marker([50.8079619327, -1.07681037068], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 62</b><br/>Baseline NO2 2015: 28.2<br/>With scheme NO2 2026: 25.6<br/>Without scheme NO2 2026: 26.2<br/>Impact: Negligible ");
	L.marker([50.8075619238, -1.07710004926], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 63</b><br/>Baseline NO2 2015: 33.6<br/>With scheme NO2 2026: 28.6<br/>Without scheme NO2 2026: 29.6<br/>Impact: Negligible ");
	L.marker([50.8055821682, -1.07662798047], {icon: caution40sIcon}).addTo(mymap2).bindPopup("<b>Receptor 64</b><br/>Baseline NO2 2015: 48.6<br/>With scheme NO2 2026: 37.6<br/>Without scheme NO2 2026: 40.1<br/>Impact: Moderate Beneficial");
	L.marker([50.8064839166, -1.07696057439], {icon: caution50sIcon}).addTo(mymap2).bindPopup("<b>Receptor 65</b><br/>Baseline NO2 2015: 52.2<br/>With scheme NO2 2026: 39.6<br/>Without scheme NO2 2026: 42.9<br/>Impact: Substantial Beneficial");
	L.marker([50.8028293548, -1.07730389714], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 66</b><br/>Baseline NO2 2015: 38.3<br/>With scheme NO2 2026: 33.1<br/>Without scheme NO2 2026: 33.1<br/>Impact: Negligible ");
	L.marker([50.8048024469, -1.07689620137], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 67</b><br/>Baseline NO2 2015: 26.1<br/>With scheme NO2 2026: 26.8<br/>Without scheme NO2 2026: 25.1<br/>Impact: Negligible ");
	L.marker([50.8014054222, -1.07747555852], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 68</b><br/>Baseline NO2 2015: 26.7<br/>With scheme NO2 2026: 25.5<br/>Without scheme NO2 2026: 24.9<br/>Impact: Negligible ");
	L.marker([50.8000356937, -1.07704640508], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 69</b><br/>Baseline NO2 2015: 26.9<br/>With scheme NO2 2026: 26.6<br/>Without scheme NO2 2026: 25.2<br/>Impact: Negligible ");
	L.marker([50.7983268672, -1.07698203206], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 70</b><br/>Baseline NO2 2015: 34.8<br/>With scheme NO2 2026: 32.9<br/>Without scheme NO2 2026: 32<br/>Impact: Slight Adverse");
	L.marker([50.7976555254, -1.07670308232], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 71</b><br/>Baseline NO2 2015: 26.4<br/>With scheme NO2 2026: 25.5<br/>Without scheme NO2 2026: 25.4<br/>Impact: Negligible ");
	L.marker([50.795797418, -1.0801470387], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 72</b><br/>Baseline NO2 2015: 27.7<br/>With scheme NO2 2026: 28<br/>Without scheme NO2 2026: 26.8<br/>Impact: Negligible ");
	L.marker([50.7971537081, -1.07971788526], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 73</b><br/>Baseline NO2 2015: 27.0<br/>With scheme NO2 2026: 26.4<br/>Without scheme NO2 2026: 25.3<br/>Impact: Negligible ");
	L.marker([50.7998119224, -1.07891322255], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 74</b><br/>Baseline NO2 2015: 28.0<br/>With scheme NO2 2026: 28<br/>Without scheme NO2 2026: 26.3<br/>Impact: Negligible ");
	L.marker([50.7996423979, -1.08247519612], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 75</b><br/>Baseline NO2 2015: 28.5<br/>With scheme NO2 2026: 27.4<br/>Without scheme NO2 2026: 25.9<br/>Impact: Negligible ");
	L.marker([50.7999407605, -1.08194948316], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 76</b><br/>Baseline NO2 2015: 28.0<br/>With scheme NO2 2026: 27.8<br/>Without scheme NO2 2026: 26.4<br/>Impact: Negligible ");
	L.marker([50.8012087805, -1.08112336278], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 77</b><br/>Baseline NO2 2015: 28.3<br/>With scheme NO2 2026: 28<br/>Without scheme NO2 2026: 26.6<br/>Impact: Negligible ");
	L.marker([50.8017241157, -1.08174563527], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 78</b><br/>Baseline NO2 2015: 30.4<br/>With scheme NO2 2026: 30.1<br/>Without scheme NO2 2026: 28.9<br/>Impact: Negligible ");
	L.marker([50.8030734532, -1.08214260221], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 79</b><br/>Baseline NO2 2015: 28.8<br/>With scheme NO2 2026: 28.6<br/>Without scheme NO2 2026: 27.5<br/>Impact: Negligible ");
	L.marker([50.8042464637, -1.0819923985], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 80</b><br/>Baseline NO2 2015: 34.0<br/>With scheme NO2 2026: 35.3<br/>Without scheme NO2 2026: 31.8<br/>Impact: Moderate Adverse");
	L.marker([50.8042464637, -1.08414889455], {icon: caution40sIcon}).addTo(mymap2).bindPopup("<b>Receptor 81</b><br/>Baseline NO2 2015: 42.7<br/>With scheme NO2 2026: 38.6<br/>Without scheme NO2 2026: 36.5<br/>Impact: Moderate Adverse");
	L.marker([50.8047482049, -1.08509303212], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 82</b><br/>Baseline NO2 2015: 35.4<br/>With scheme NO2 2026: 34.3<br/>Without scheme NO2 2026: 33.3<br/>Impact: Slight Adverse");
	L.marker([50.8033650135, -1.0843420136], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 83</b><br/>Baseline NO2 2015: 38.1<br/>With scheme NO2 2026: 35.6<br/>Without scheme NO2 2026: 36<br/>Impact: Negligible ");
	L.marker([50.8055550477, -1.07761503339], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 84</b><br/>Baseline NO2 2015: 33.5<br/>With scheme NO2 2026: 29.7<br/>Without scheme NO2 2026: 30<br/>Impact: Negligible ");
	L.marker([50.8053313027, -1.07932091832], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 85</b><br/>Baseline NO2 2015: 28.8<br/>With scheme NO2 2026: 26.8<br/>Without scheme NO2 2026: 25.8<br/>Impact: Negligible ");
	L.marker([50.8037243828, -1.08760359049], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 86</b><br/>Baseline NO2 2015: 29.6<br/>With scheme NO2 2026: 28.4<br/>Without scheme NO2 2026: 26.6<br/>Impact: Negligible ");
	L.marker([50.803724376, -1.08708859563], {icon: caution50sIcon}).addTo(mymap2).bindPopup("<b>Receptor 87</b><br/>Baseline NO2 2015: 51.6<br/>With scheme NO2 2026: 40.3<br/>Without scheme NO2 2026: 44.2<br/>Impact: Substantial Beneficial");
	L.marker([50.8002933685, -1.09369755864], {icon: caution40sIcon}).addTo(mymap2).bindPopup("<b>Receptor 88</b><br/>Baseline NO2 2015: 48.1<br/>With scheme NO2 2026: 41.3<br/>Without scheme NO2 2026: 41.3<br/>Impact: Negligible ");
	L.marker([50.8000153509, -1.09107972264], {icon: caution40sIcon}).addTo(mymap2).bindPopup("<b>Receptor 89</b><br/>Baseline NO2 2015: 49.0<br/>With scheme NO2 2026: 41.4<br/>Without scheme NO2 2026: 37.5<br/>Impact: Substantial Adverse");
	L.marker([50.7993508144, -1.09116555333], {icon: caution40sIcon}).addTo(mymap2).bindPopup("<b>Receptor 90</b><br/>Baseline NO2 2015: 44.8<br/>With scheme NO2 2026: 37.9<br/>Without scheme NO2 2026: 33.2<br/>Impact: Substantial Adverse");
	L.marker([50.7993440334, -1.092495929], {icon: caution50sIcon}).addTo(mymap2).bindPopup("<b>Receptor 91</b><br/>Baseline NO2 2015: 53.0<br/>With scheme NO2 2026: 39.6<br/>Without scheme NO2 2026: 37.9<br/>Impact: Moderate Adverse");
	L.marker([50.7960415531, -1.09437347531], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 92</b><br/>Baseline NO2 2015: 38.3<br/>With scheme NO2 2026: 36.2<br/>Without scheme NO2 2026: 30.8<br/>Impact: Moderate Adverse");
	L.marker([50.8064093366, -1.08713151097], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 93</b><br/>Baseline NO2 2015: 36.7<br/>With scheme NO2 2026: 33.6<br/>Without scheme NO2 2026: 33.6<br/>Impact: Negligible ");
	L.marker([50.8059143933, -1.08520032048], {icon: caution50sIcon}).addTo(mymap2).bindPopup("<b>Receptor 94</b><br/>Baseline NO2 2015: 53.9<br/>With scheme NO2 2026: 39.3<br/>Without scheme NO2 2026: 48.1<br/>Impact: Substantial Beneficial");
	L.marker([50.8076839608, -1.08262539983], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 95</b><br/>Baseline NO2 2015: 29.9<br/>With scheme NO2 2026: 26.2<br/>Without scheme NO2 2026: 28.4<br/>Impact: Slight Beneficial");
	L.marker([50.8073856476, -1.07851625562], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 96</b><br/>Baseline NO2 2015: 24.0<br/>With scheme NO2 2026: 24.7<br/>Without scheme NO2 2026: 23.7<br/>Impact: Negligible ");
	L.marker([50.8084432949, -1.08272195935], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 97</b><br/>Baseline NO2 2015: 23.6<br/>With scheme NO2 2026: 23.4<br/>Without scheme NO2 2026: 22.9<br/>Impact: Negligible ");
	L.marker([50.8106737674, -1.08203531384], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 98</b><br/>Baseline NO2 2015: 24.0<br/>With scheme NO2 2026: 25.6<br/>Without scheme NO2 2026: 23.9<br/>Impact: Negligible ");
	L.marker([50.8112703499, -1.0796427834], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 99</b><br/>Baseline NO2 2015: 23.6<br/>With scheme NO2 2026: 25.1<br/>Without scheme NO2 2026: 23.5<br/>Impact: Negligible ");
	L.marker([50.8120906451, -1.08230354548], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 100</b><br/>Baseline NO2 2015: 30.5<br/>With scheme NO2 2026: 28<br/>Without scheme NO2 2026: 27.6<br/>Impact: Negligible ");
	L.marker([50.8132566502, -1.08208896875], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 101</b><br/>Baseline NO2 2015: 24.3<br/>With scheme NO2 2026: 25.2<br/>Without scheme NO2 2026: 23.7<br/>Impact: Negligible ");
	L.marker([50.8128227913, -1.0800504899], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 102</b><br/>Baseline NO2 2015: 32.3<br/>With scheme NO2 2026: 34.6<br/>Without scheme NO2 2026: 30.1<br/>Impact: Moderate Adverse");
	L.marker([50.8132566502, -1.08071567774], {icon: caution40sIcon}).addTo(mymap2).bindPopup("<b>Receptor 103</b><br/>Baseline NO2 2015: 49.0<br/>With scheme NO2 2026: 40.7<br/>Without scheme NO2 2026: 41.5<br/>Impact: Moderate Beneficial");
	L.marker([50.8135142521, -1.08000757456], {icon: caution40sIcon}).addTo(mymap2).bindPopup("<b>Receptor 104</b><br/>Baseline NO2 2015: 43.1<br/>With scheme NO2 2026: 38.2<br/>Without scheme NO2 2026: 37.4<br/>Impact: Moderate Adverse");
	L.marker([50.8138396419, -1.07807638407], {icon: caution50sIcon}).addTo(mymap2).bindPopup("<b>Receptor 105</b><br/>Baseline NO2 2015: 56.0<br/>With scheme NO2 2026: 44.4<br/>Without scheme NO2 2026: 46.1<br/>Impact: Substantial Beneficial");
	L.marker([50.8140972405, -1.07063057184], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 106</b><br/>Baseline NO2 2015: 27.6<br/>With scheme NO2 2026: 29.2<br/>Without scheme NO2 2026: 26.6<br/>Impact: Slight Adverse");
	L.marker([50.811812691, -1.07973934293], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 107</b><br/>Baseline NO2 2015: 26.1<br/>With scheme NO2 2026: 27.7<br/>Without scheme NO2 2026: 26.7<br/>Impact: Negligible ");
	L.marker([50.8119347168, -1.07776523709], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 108</b><br/>Baseline NO2 2015: 30.8<br/>With scheme NO2 2026: 29.3<br/>Without scheme NO2 2026: 28.3<br/>Impact: Negligible ");
	L.marker([50.8169171681, -1.0751795876], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 109</b><br/>Baseline NO2 2015: 24.0<br/>With scheme NO2 2026: 24.2<br/>Without scheme NO2 2026: 23.6<br/>Impact: Negligible ");
	L.marker([50.8169646174, -1.07943893552], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 110</b><br/>Baseline NO2 2015: 22.7<br/>With scheme NO2 2026: 23.2<br/>Without scheme NO2 2026: 22.8<br/>Impact: Negligible ");
	L.marker([50.8139616625, -1.06530906916], {icon: caution40sIcon}).addTo(mymap2).bindPopup("<b>Receptor 111</b><br/>Baseline NO2 2015: 43.1<br/>With scheme NO2 2026: 40<br/>Without scheme NO2 2026: 38.3<br/>Impact: Moderate Adverse");
	L.marker([50.8142463759, -1.06451513529], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 112</b><br/>Baseline NO2 2015: 26.3<br/>With scheme NO2 2026: 26.7<br/>Without scheme NO2 2026: 25.7<br/>Impact: Negligible ");
	L.marker([50.8170663012, -1.06402160883], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 113</b><br/>Baseline NO2 2015: 36.2<br/>With scheme NO2 2026: 33.2<br/>Without scheme NO2 2026: 32.2<br/>Impact: Slight Adverse");
	L.marker([50.8173103252, -1.06427910089], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 114</b><br/>Baseline NO2 2015: 29.8<br/>With scheme NO2 2026: 27.7<br/>Without scheme NO2 2026: 26.8<br/>Impact: Negligible ");
	L.marker([50.8198725002, -1.06466533899], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 115</b><br/>Baseline NO2 2015: 33.1<br/>With scheme NO2 2026: 29.7<br/>Without scheme NO2 2026: 29<br/>Impact: Negligible ");
	L.marker([50.8221092046, -1.06490137339], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 116</b><br/>Baseline NO2 2015: 28.8<br/>With scheme NO2 2026: 27.4<br/>Without scheme NO2 2026: 26.1<br/>Impact: Negligible ");
	L.marker([50.8173916663, -1.07929947138], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 117</b><br/>Baseline NO2 2015: 28.7<br/>With scheme NO2 2026: 26.6<br/>Without scheme NO2 2026: 26.1<br/>Impact: Negligible ");
	L.marker([50.8175814615, -1.08017923594], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 118</b><br/>Baseline NO2 2015: 32.4<br/>With scheme NO2 2026: 32.8<br/>Without scheme NO2 2026: 29.3<br/>Impact: Moderate Adverse");
	L.marker([50.8192353587, -1.08086588144], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 119</b><br/>Baseline NO2 2015: 24.8<br/>With scheme NO2 2026: 25<br/>Without scheme NO2 2026: 24.1<br/>Impact: Negligible ");
	L.marker([50.8220956492, -1.07556583643], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 120</b><br/>Baseline NO2 2015: 23.9<br/>With scheme NO2 2026: 24.2<br/>Without scheme NO2 2026: 23.6<br/>Impact: Negligible ");
	L.marker([50.8170256304, -1.08322622538], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 121</b><br/>Baseline NO2 2015: 26.4<br/>With scheme NO2 2026: 25.3<br/>Without scheme NO2 2026: 24.7<br/>Impact: Negligible ");
	L.marker([50.8146937793, -1.0847497201], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 122</b><br/>Baseline NO2 2015: 27.2<br/>With scheme NO2 2026: 29.1<br/>Without scheme NO2 2026: 27<br/>Impact: Negligible ");
	L.marker([50.8183406347, -1.08350517511], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 123</b><br/>Baseline NO2 2015: 31.9<br/>With scheme NO2 2026: 34.8<br/>Without scheme NO2 2026: 30.9<br/>Impact: Moderate Adverse");
	L.marker([50.815791933, -1.08494283915], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 124</b><br/>Baseline NO2 2015: 23.9<br/>With scheme NO2 2026: 24.1<br/>Without scheme NO2 2026: 23.3<br/>Impact: Negligible ");
	L.marker([50.8077585388, -1.08687401891], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 125</b><br/>Baseline NO2 2015: 29.3<br/>With scheme NO2 2026: 31.1<br/>Without scheme NO2 2026: 28.2<br/>Impact: Moderate Adverse");
	L.marker([50.8121448718, -1.08511448979], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 126</b><br/>Baseline NO2 2015: 34.2<br/>With scheme NO2 2026: 34.9<br/>Without scheme NO2 2026: 31.8<br/>Impact: Moderate Adverse");
	L.marker([50.815758033, -1.08810783505], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 127</b><br/>Baseline NO2 2015: 29.8<br/>With scheme NO2 2026: 31.8<br/>Without scheme NO2 2026: 28.3<br/>Impact: Moderate Adverse");
	L.marker([50.821743199, -1.09201313138], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 128</b><br/>Baseline NO2 2015: 37.6<br/>With scheme NO2 2026: 39.3<br/>Without scheme NO2 2026: 35.1<br/>Impact: Substantial Adverse");
	L.marker([50.8348224949, -1.05498792887], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 129</b><br/>Baseline NO2 2015: 31.2<br/>With scheme NO2 2026: 31<br/>Without scheme NO2 2026: 30<br/>Impact: Slight Adverse");
	L.marker([50.8373972599, -1.06934311152], {icon: caution50sIcon}).addTo(mymap2).bindPopup("<b>Receptor 130</b><br/>Baseline NO2 2015: 51.1<br/>With scheme NO2 2026: 47.9<br/>Without scheme NO2 2026: 49.3<br/>Impact: Substantial Beneficial");
	L.marker([50.8341177976, -1.07213260889], {icon: caution40sIcon}).addTo(mymap2).bindPopup("<b>Receptor 131</b><br/>Baseline NO2 2015: 40.3<br/>With scheme NO2 2026: 37.7<br/>Without scheme NO2 2026: 38.3<br/>Impact: Moderate Beneficial");
	L.marker([50.8321391675, -1.07384922266], {icon: caution30sIcon}).addTo(mymap2).bindPopup("<b>Receptor 132</b><br/>Baseline NO2 2015: 31.1<br/>With scheme NO2 2026: 28.8<br/>Without scheme NO2 2026: 27.8<br/>Impact: Negligible ");
	L.marker([50.8307703401, -1.07417108774], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 133</b><br/>Baseline NO2 2015: 20.8<br/>With scheme NO2 2026: 20.6<br/>Without scheme NO2 2026: 20.2<br/>Impact: Negligible ");
	L.marker([50.826921145, -1.07653143167], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 134</b><br/>Baseline NO2 2015: 26.8<br/>With scheme NO2 2026: 26.9<br/>Without scheme NO2 2026: 26.3<br/>Impact: Negligible ");
	L.marker([50.8249964283, -1.07880594492], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 135</b><br/>Baseline NO2 2015: 26.9<br/>With scheme NO2 2026: 27.1<br/>Without scheme NO2 2026: 26.5<br/>Impact: Negligible ");
	L.marker([50.8230716324, -1.08230354548], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 136</b><br/>Baseline NO2 2015: 22.9<br/>With scheme NO2 2026: 23<br/>Without scheme NO2 2026: 22.5<br/>Impact: Negligible ");
	L.marker([50.8425328076, -1.07015849233], {icon: caution20sIcon}).addTo(mymap2).bindPopup("<b>Receptor 137</b><br/>Baseline NO2 2015: 21.5<br/>With scheme NO2 2026: 21.1<br/>Without scheme NO2 2026: 20.6<br/>Impact: Negligible ");

</script>



</body>
</html>

