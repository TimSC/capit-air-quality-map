<!DOCTYPE html>
<html>
<head>
	
	<title>Portsmouth NO2 measured levels 2019</title>

	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<link rel="stylesheet" href="leaflet/leaflet.css"/>
	<script src="leaflet/leaflet.js"></script>
	
</head>
<body>

<div id="mapid" style="width: 100%; height: 500px;"></div>

<script>

	var mymap3 = L.map('mapid').setView([50.80, -1.083333], 13);

	/*L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoidGltc2MiLCJhIjoiY2l0cmdpeXBoMDAwcDJ1cGtvdXNhN2FxbiJ9.9RHQRB-C3ZPZhRNRs5TYFA', {
		maxZoom: 18,
		attribution: 'Map data &copy; <a href="http://www.openstreetmap.org/about/">OpenStreetMap</a> contributors, ' +
			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		id: 'mapbox.streets'
	}).addTo(mymap3);*/
	
	L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoidGltc2MiLCJhIjoiY2l0cmdpeXBoMDAwcDJ1cGtvdXNhN2FxbiJ9.9RHQRB-C3ZPZhRNRs5TYFA', {
	attribution: '© <a href="https://www.mapbox.com/about/maps/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> <strong><a href="https://www.mapbox.com/map-feedback/" target="_blank">Improve this map</a></strong>',
	tileSize: 512,
	maxZoom: 18,
	zoomOffset: -1,
	id: 'mapbox/streets-v11',
	accessToken: 'pk.eyJ1IjoidGltc2MiLCJhIjoiY2l0cmdpeXBoMDAwcDJ1cGtvdXNhN2FxbiJ9.9RHQRB-C3ZPZhRNRs5TYFA'
	}).addTo(mymap3);

	var caution0sIcon = L.icon({
		iconUrl: 'caution-0s.png',

		iconSize:	 [13, 13], // size of the icon
		iconAnchor:   [6, 6], // point of the icon which will correspond to marker's location
		popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
	});

	var caution10sIcon = L.icon({
		iconUrl: 'caution-10s.png',

		iconSize:	 [26, 26], // size of the icon
		iconAnchor:   [13, 13], // point of the icon which will correspond to marker's location
		popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
	});

	var caution20sIcon = L.icon({
		iconUrl: 'caution-20s.png',

		iconSize:	 [38, 38], // size of the icon
		iconAnchor:   [19, 19], // point of the icon which will correspond to marker's location
		popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
	});

	var caution30sIcon = L.icon({
		iconUrl: 'caution-30s.png',

		iconSize:	 [51, 51], // size of the icon
		iconAnchor:   [25, 25], // point of the icon which will correspond to marker's location
		popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
	});

	var caution40sIcon = L.icon({
		iconUrl: 'caution-40s.png',

		iconSize:	 [64, 64], // size of the icon
		iconAnchor:   [32, 32], // point of the icon which will correspond to marker's location
		popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
	});

	var caution50sIcon = L.icon({
		iconUrl: 'caution-50s.png',

		iconSize:	 [77, 77], // size of the icon
		iconAnchor:   [38, 38], // point of the icon which will correspond to marker's location
		popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
	});

	L.marker([50.7949094203875, -1.09511006592172], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 1</b><br/>NO2 2012: 42.54<br/>NO2 2013: 41.9<br/>NO2 2014: 42.57<br/>NO2 2015: 44.33<br/>NO2 2016: 43.52<br/>NO2 2017: 38.8<br/>NO2 2018: 42.92<br/>NO2 2019: 36.92<br/>NO2 2020: 29.67");
	L.marker([50.7904050607429, -1.09756636788069], {icon: caution10sIcon}).addTo(mymap3).bindPopup("<b>Receptor 2</b><br/>NO2 2012: 17.48<br/>NO2 2013: 16.5<br/>NO2 2014: 16.55<br/>NO2 2015: 15.74<br/>NO2 2016: 17.4<br/>NO2 2017: 16.38<br/>NO2 2018: 17.09<br/>NO2 2019: 14.96<br/>NO2 2020: 12.95");
	L.marker([50.7912378405864, -1.10176415235435], {icon: caution10sIcon}).addTo(mymap3).bindPopup("<b>Receptor 3</b><br/>NO2 2012: 26.63<br/>NO2 2013: 22.1<br/>NO2 2014: 25.67<br/>NO2 2015: 24.07<br/>NO2 2016: 25.75<br/>NO2 2017: 23.7<br/>NO2 2018: 24.13<br/>NO2 2019: 21.02<br/>NO2 2020: 18.29");
	L.marker([50.7996237383411, -1.10469691450652], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 4</b><br/>NO2 2012: 36.35<br/>NO2 2013: 31.51<br/>NO2 2014: 27.97<br/>NO2 2015: 30.54<br/>NO2 2016: 34.7<br/>NO2 2017: 34.2<br/>NO2 2018: 34.04<br/>NO2 2019: 31.2<br/>NO2 2020: 27.15");
	L.marker([50.8157302541601, -1.08962586276778], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 5</b><br/>NO2 2012: 28.62<br/>NO2 2013: 27.49<br/>NO2 2014: 28.93<br/>NO2 2015: 27.53<br/>NO2 2016: 29.52<br/>NO2 2017: 24.48<br/>NO2 2018: 28.08<br/>NO2 2019: 24.86<br/>NO2 2020: 22.28");
	L.marker([50.8157460255979, -1.08819182271118], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 6</b><br/>NO2 2012: 35.62<br/>NO2 2013: 38.29<br/>NO2 2014: 34.85<br/>NO2 2015: 46.06<br/>NO2 2016: 36.08<br/>NO2 2017: 32.08<br/>NO2 2018: 30.86<br/>NO2 2019: 30.18<br/>NO2 2020: 21.85");
	L.marker([50.8164877662495, -1.08874520288376], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 7</b><br/>NO2 2012: 29.78<br/>NO2 2013: 30<br/>NO2 2014: 26.53<br/>NO2 2015: 26.05<br/>NO2 2016: 28.09<br/>NO2 2017: 27.32<br/>NO2 2018: 27.74<br/>NO2 2019: 23.29<br/>NO2 2020: 22.73");
	L.marker([50.8348836655246, -1.05431886384767], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 8</b><br/>NO2 2012: 28.81<br/>NO2 2013: 27.22<br/>NO2 2014: 28.37<br/>NO2 2015: 28.43<br/>NO2 2016: 29.94<br/>NO2 2017: 26.75<br/>NO2 2018: 25.97<br/>NO2 2019: 23.18<br/>NO2 2020: 21.74");
	L.marker([50.8455525862927, -1.06928707865332], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 9</b><br/>NO2 2012: 35.07<br/>NO2 2013: 31.95<br/>NO2 2014: 33.88<br/>NO2 2015: 34.98<br/>NO2 2016: 40.86<br/>NO2 2017: 37.06<br/>NO2 2018: 36.7<br/>NO2 2019: 33.6<br/>NO2 2020: 29.72");
	L.marker([50.839286849303, -1.04830819438317], {icon: caution10sIcon}).addTo(mymap3).bindPopup("<b>Receptor 10</b><br/>NO2 2012: 17.91<br/>NO2 2013: 17.66<br/>NO2 2014: 16.66<br/>NO2 2015: 16.48<br/>NO2 2016: 19.54<br/>NO2 2017: 17.58<br/>NO2 2018: 17.17<br/>NO2 2019: 15.08<br/>NO2 2020: 14.39");
	L.marker([50.8267881030592, -1.05194030689803], {icon: caution10sIcon}).addTo(mymap3).bindPopup("<b>Receptor 11</b><br/>NO2 2012: 31.76<br/>NO2 2013: 29.54<br/>NO2 2014: 33.29<br/>NO2 2015: 28.27<br/>NO2 2016: 28.1<br/>NO2 2017: 23.5<br/>NO2 2018: 22.9<br/>NO2 2019: 20.7<br/>NO2 2020: 18.8");
	L.marker([50.8293840779691, -1.06267965759766], {icon: caution10sIcon}).addTo(mymap3).bindPopup("<b>Receptor 14</b><br/>NO2 2012: 22.68<br/>NO2 2013: 21.61<br/>NO2 2014: 27.21<br/>NO2 2015: 26.87<br/>NO2 2016: 22.2<br/>NO2 2017: 21.28<br/>NO2 2018: 21.66<br/>NO2 2019: 19.54<br/>NO2 2020: 17");
	L.marker([50.8076952371813, -1.0629573550303], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 15</b><br/>NO2 2012: 28.82<br/>NO2 2013: 28.15<br/>NO2 2014: 27.57<br/>NO2 2015: 26.21<br/>NO2 2016: 28.97<br/>NO2 2017: 28.95<br/>NO2 2018: 27.64<br/>NO2 2019: 24.91<br/>NO2 2020: 21.16");
	L.marker([50.8336738616781, -1.07161127882214], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 16</b><br/>NO2 2012: 36.44<br/>NO2 2013: 33.98<br/>NO2 2014: 32.32<br/>NO2 2015: 32.01<br/>NO2 2016: 36.45<br/>NO2 2017: 35.44<br/>NO2 2018: 29.59<br/>NO2 2019: 25.44<br/>NO2 2020: 20.94");
	L.marker([50.8077697909407, -1.06328230211853], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 18</b><br/>NO2 2012: 29.52<br/>NO2 2013: 27.8<br/>NO2 2014: 28.9<br/>NO2 2015: 26.91<br/>NO2 2016: 29.3<br/>NO2 2017: 29.62<br/>NO2 2018: 26.01<br/>NO2 2019: 24.32<br/>NO2 2020: 22.77");
	L.marker([50.7977914690403, -1.05929577867045], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 19</b><br/>NO2 2012: 34.52<br/>NO2 2013: 30.1<br/>NO2 2014: 37.24<br/>NO2 2015: 35.08<br/>NO2 2016: 39.61<br/>NO2 2017: 34.72<br/>NO2 2018: 37.68<br/>NO2 2019: 33.38<br/>NO2 2020: 28.59");
	L.marker([50.7904626949156, -1.05490282405225], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 20</b><br/>NO2 2012: 26.07<br/>NO2 2013: 27.42<br/>NO2 2014: 28.9<br/>NO2 2015: 27.58<br/>NO2 2016: 29.12<br/>NO2 2017: 29.73<br/>NO2 2018: 28.42<br/>NO2 2019: 24.01<br/>NO2 2020: 21.79");
	L.marker([50.7865784277977, -1.07630350536366], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 21</b><br/>NO2 2012: 35.79<br/>NO2 2013: 32.88<br/>NO2 2014: 35.18<br/>NO2 2015: 35.28<br/>NO2 2016: 40.05<br/>NO2 2017: 38.37<br/>NO2 2018: 36.5<br/>NO2 2019: 33.41<br/>NO2 2020: 28.44");
	L.marker([50.7897018007222, -1.08235676161189], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 22</b><br/>NO2 2012: 31.61<br/>NO2 2013: 28.69<br/>NO2 2014: 30.8<br/>NO2 2015: 28.06<br/>NO2 2016: 31.23<br/>NO2 2017: 26.48<br/>NO2 2018: 29.28<br/>NO2 2019: 24.49<br/>NO2 2020: 21.96");
	L.marker([50.7938159555249, -1.07949522118198], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 23</b><br/>NO2 2012: 41.05<br/>NO2 2013: 30.4<br/>NO2 2014: 28.8<br/>NO2 2015: 31<br/>NO2 2016: 37<br/>NO2 2017: 34<br/>NO2 2018: 34.6<br/>NO2 2019: 32.2<br/>NO2 2020: 27.3");
	L.marker([50.8025313178885, -1.07737976203406], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 24</b><br/>NO2 2012: 39.12<br/>NO2 2013: 42.48<br/>NO2 2014: 40.49<br/>NO2 2015: 36.32<br/>NO2 2016: 37.74<br/>NO2 2017: 38.3<br/>NO2 2018: 36.76<br/>NO2 2019: 31.3<br/>NO2 2020: 28.7");
	L.marker([50.809822909082, -1.0783006338555], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 25</b><br/>NO2 2012: 44.58<br/>NO2 2013: 38.69<br/>NO2 2014: 52.18<br/>NO2 2015: 41.79<br/>NO2 2016: 43.65<br/>NO2 2017: 44.28<br/>NO2 2018: 38.21<br/>NO2 2019: 37.63<br/>NO2 2020: 30.62");
	L.marker([50.813695519258, -1.08015490115732], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 26</b><br/>NO2 2012: 50.48<br/>NO2 2013: 50.93<br/>NO2 2014: 40.81<br/>NO2 2015: 43.12<br/>NO2 2016: 49.16<br/>NO2 2017: 43.09<br/>NO2 2018: 46.02<br/>NO2 2019: 40.42<br/>NO2 2020: 36.51");
	L.marker([50.8090759634263, -1.08623523032718], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 30</b><br/>NO2 2012: 37.97<br/>NO2 2013: 38.83<br/>NO2 2014: 44.12<br/>NO2 2015: 34.31<br/>NO2 2016: 39.34<br/>NO2 2017: 38.48<br/>NO2 2018: 39.17<br/>NO2 2019: 34.29<br/>NO2 2020: 28.2");
	L.marker([50.8040105818591, -1.08708620794113], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 34</b><br/>NO2 2012: 38.8<br/>NO2 2013: 34.65<br/>NO2 2014: 35.52<br/>NO2 2015: 34.65<br/>NO2 2016: 36.06<br/>NO2 2017: 36.17<br/>NO2 2018: 33.34<br/>NO2 2019: 31.01<br/>NO2 2020: 25.26");
	L.marker([50.7938792541783, -1.09562656471484], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 35</b><br/>NO2 2012: 31.1<br/>NO2 2013: 28.96<br/>NO2 2014: 41.42<br/>NO2 2015: 28.48<br/>NO2 2016: 30.68<br/>NO2 2017: 30.13<br/>NO2 2018: 30.08<br/>NO2 2019: 26.92<br/>NO2 2020: 21.36");
	L.marker([50.7899394630902, -1.08628206396462], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 36</b><br/>NO2 2012: 32.84<br/>NO2 2013: 30.33<br/>NO2 2014: 34.81<br/>NO2 2015: 29<br/>NO2 2016: 33.32<br/>NO2 2017: 29.74<br/>NO2 2018: 31.47<br/>NO2 2019: 27.01<br/>NO2 2020: 20.88");
	L.marker([50.8134106655738, -1.08510024423317], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 42</b><br/>NO2 2018: 38.05<br/>NO2 2019: 32.46<br/>NO2 2020: 28.41");
	L.marker([50.8132240750985, -1.08195268399636], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 43</b><br/>NO2 2018: 32.5<br/>NO2 2019: 30.3<br/>NO2 2020: 27.7");
	L.marker([50.8034809673648, -1.08835958711967], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 44</b><br/>NO2 2018: 40.41<br/>NO2 2019: 32.35<br/>NO2 2020: 27.49");
	L.marker([50.8032552978882, -1.08825044600659], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 45</b><br/>NO2 2018: 41.97<br/>NO2 2019: 31.84<br/>NO2 2020: 27.61");
	L.marker([50.8074369339206, -1.08824001412653], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 46</b><br/>NO2 2018: 44.51<br/>NO2 2019: 33.87<br/>NO2 2020: 28.86");
	L.marker([50.8150702867719, -1.08458519649296], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 47</b><br/>NO2 2018: 36.77<br/>NO2 2019: 31.07<br/>NO2 2020: 27.87");
	L.marker([50.8150151119374, -1.08443012833719], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 48</b><br/>NO2 2018: 30.54<br/>NO2 2019: 25.32<br/>NO2 2020: 29.29");
	L.marker([50.7989654843892, -1.10680965471463], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 49</b><br/>NO2 2018: 34.64<br/>NO2 2019: 29.05<br/>NO2 2020: 22.17");
	L.marker([50.7996740596149, -1.10188623420277], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 50</b><br/>NO2 2018: 40.37<br/>NO2 2019: 34.07<br/>NO2 2020: 24.7");
	L.marker([50.799653098735, -1.10266711311825], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 51</b><br/>NO2 2018: 33.18<br/>NO2 2019: 28.92<br/>NO2 2020: 23.12");
	L.marker([50.7998166448617, -1.10405465385601], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 52</b><br/>NO2 2018: 32.29<br/>NO2 2019: 27.77<br/>NO2 2020: 23.5");
	L.marker([50.7924268151331, -1.10435201151267], {icon: caution10sIcon}).addTo(mymap3).bindPopup("<b>Receptor 55</b><br/>NO2 2017: 30.4<br/>NO2 2018: 25.38<br/>NO2 2019: 26.17<br/>NO2 2020: 19.57");
	L.marker([50.7941491557629, -1.1037941065971], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 56</b><br/>NO2 2017: 36.17<br/>NO2 2018: 35.09<br/>NO2 2019: 30.44<br/>NO2 2020: 24.78");
	L.marker([50.7930185093283, -1.10060913191781], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 58</b><br/>NO2 2017: 33.8<br/>NO2 2018: 29.32<br/>NO2 2019: 26.93<br/>NO2 2020: 20.74");
	L.marker([50.798777305545, -1.06110652905209], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 59</b><br/>NO2 2018: 38.23<br/>NO2 2019: 37.11<br/>NO2 2020: 31.53");
	L.marker([50.8000791534954, -1.06196026071694], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 60</b><br/>NO2 2018: 29.77<br/>NO2 2019: 25.16<br/>NO2 2020: 23.81");
	L.marker([50.8012734470017, -1.06285876392314], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 61</b><br/>NO2 2018: 33.67<br/>NO2 2019: 30.28<br/>NO2 2020: 23.76");
	L.marker([50.8009374608964, -1.06245394827905], {icon: caution10sIcon}).addTo(mymap3).bindPopup("<b>Receptor 62</b><br/>NO2 2018: 22.04<br/>NO2 2019: 17.56<br/>NO2 2020: 16.78");
	L.marker([50.7973102784706, -1.05984465250638], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 63</b><br/>NO2 2018: 34.17<br/>NO2 2019: 29.43<br/>NO2 2020: 23.83");
	L.marker([50.7972505382327, -1.06024316556025], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 64</b><br/>NO2 2018: 37.88<br/>NO2 2019: 30.33<br/>NO2 2020: 26.43");
	L.marker([50.7990800675555, -1.05516890792182], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 65</b><br/>NO2 2017: 27.62<br/>NO2 2018: 28.24<br/>NO2 2019: 24.35<br/>NO2 2020: 20.79");
	L.marker([50.7977158498377, -1.06107103933047], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 66</b><br/>NO2 2018: 31.9<br/>NO2 2019: 27.6<br/>NO2 2020: 23.33");
	L.marker([50.7980267927118, -1.05836871594406], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 67</b><br/>NO2 2018: 36.73<br/>NO2 2019: 31.44<br/>NO2 2020: 25.54");
	L.marker([50.7982375500562, -1.05774011594381], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 68</b><br/>NO2 2018: 36.86<br/>NO2 2019: 29.25<br/>NO2 2020: 24.71");
	L.marker([50.7979888263324, -1.05923505619304], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 69</b><br/>NO2 2018: 31.14<br/>NO2 2019: 24.95<br/>NO2 2020: 23.68");
	L.marker([50.7916457363522, -1.05551743125519], {icon: caution10sIcon}).addTo(mymap3).bindPopup("<b>Receptor 70</b><br/>NO2 2017: 23.69<br/>NO2 2018: 25.14<br/>NO2 2019: 21.58<br/>NO2 2020: 19.59");
	L.marker([50.8464054823978, -1.06799167023465], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 71</b><br/>NO2 2018: 27.78<br/>NO2 2019: 25.19<br/>NO2 2020: 21.97");
	L.marker([50.8459890477528, -1.06876701506697], {icon: caution10sIcon}).addTo(mymap3).bindPopup("<b>Receptor 72</b><br/>NO2 2018: 26.49<br/>NO2 2019: 23.33<br/>NO2 2020: 19.33");
	L.marker([50.845692816493, -1.06882975909215], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 73</b><br/>NO2 2018: 27.4<br/>NO2 2019: 23.78<br/>NO2 2020: 20.24");
	L.marker([50.8442502212491, -1.06946933578649], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 74</b><br/>NO2 2018: 37.27<br/>NO2 2019: 30.25<br/>NO2 2020: 30.72");
	L.marker([50.8463710567443, -1.06931333557617], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 75</b><br/>NO2 2018: 25.71<br/>NO2 2019: 21.25<br/>NO2 2020: 20.19");
	L.marker([50.8142634840374, -1.0645009301141], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 76</b><br/>NO2 2018: 31.25<br/>NO2 2019: 28.87<br/>NO2 2020: 23.55");
	L.marker([50.8146584125502, -1.06440786981852], {icon: caution10sIcon}).addTo(mymap3).bindPopup("<b>Receptor 77</b><br/>NO2 2018: 21.23<br/>NO2 2019: 18.51<br/>NO2 2020: 19.01");
	L.marker([50.7921388126548, -1.05755060250579], {icon: caution10sIcon}).addTo(mymap3).bindPopup("<b>Receptor 78</b><br/>NO2 2018: 25.04<br/>NO2 2019: 19.91<br/>NO2 2020: 17.88");
	L.marker([50.7867048724896, -1.0763719465793], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 80</b><br/>NO2 2018: 38.35<br/>NO2 2019: 32.36<br/>NO2 2020: 23.99");
	L.marker([50.7866066399457, -1.07532407654687], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 81</b><br/>NO2 2018: 35.22<br/>NO2 2019: 30.66<br/>NO2 2020: 22.31");
	L.marker([50.78641107663, -1.07674658480298], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 82</b><br/>NO2 2018: 30.79<br/>NO2 2019: 26.61<br/>NO2 2020: 22.15");
	L.marker([50.7867451058397, -1.07691024475982], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 83</b><br/>NO2 2018: 32.43<br/>NO2 2019: 28.5<br/>NO2 2020: 21.13");
	L.marker([50.7868673960452, -1.07645386392378], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 84</b><br/>NO2 2018: 42.82<br/>NO2 2019: 30.4<br/>NO2 2020: 23.81");
	L.marker([50.7866210274925, -1.07713967333201], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 85</b><br/>NO2 2018: 40.41<br/>NO2 2019: 31.52<br/>NO2 2020: 25.06");
	L.marker([50.7935027576867, -1.07628054989761], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 86</b><br/>NO2 2018: 28.89<br/>NO2 2019: 24<br/>NO2 2020: 24.33");
	L.marker([50.7950333309985, -1.07650578270844], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 87</b><br/>NO2 2018: 27.3<br/>NO2 2019: 24.8<br/>NO2 2020: 20.23");
	L.marker([50.7868687435914, -1.07662407707686], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 88</b><br/>NO2 2018: 35.35<br/>NO2 2019: 28.43<br/>NO2 2020: 27.25");
	L.marker([50.7864187209217, -1.07657619616837], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 89</b><br/>NO2 2018: 30.85<br/>NO2 2019: 25.97<br/>NO2 2020: 20.54");
	L.marker([50.8031033941103, -1.06340401093597], {icon: caution10sIcon}).addTo(mymap3).bindPopup("<b>Receptor 90</b><br/>NO2 2018: 23.98<br/>NO2 2019: 22.13<br/>NO2 2020: 18.23");
	L.marker([50.8031601910885, -1.06375766503334], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 91</b><br/>NO2 2018: 26.69<br/>NO2 2019: 23.84<br/>NO2 2020: 21.54");
	L.marker([50.7933704045412, -1.0574974567088], {icon: caution10sIcon}).addTo(mymap3).bindPopup("<b>Receptor 92</b><br/>NO2 2017: 28.69<br/>NO2 2018: 27.27<br/>NO2 2019: 25.7<br/>NO2 2020: 19.26");
	L.marker([50.7914407787724, -1.08164168712153], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 93</b><br/>NO2 2018: 35.04<br/>NO2 2019: 34.81<br/>NO2 2020: 30.46");
	L.marker([50.7959497749414, -1.07753773609562], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 95</b><br/>NO2 2018: 29.31<br/>NO2 2019: 26.06<br/>NO2 2020: 22.57");
	L.marker([50.7863068332478, -1.07267713436434], {icon: caution10sIcon}).addTo(mymap3).bindPopup("<b>Receptor 96</b><br/>NO2 2018: 23.47<br/>NO2 2019: 21.48<br/>NO2 2020: 18.9");
	L.marker([50.7944852140773, -1.06639987608297], {icon: caution10sIcon}).addTo(mymap3).bindPopup("<b>Receptor 97</b><br/>NO2 2018: 25.84<br/>NO2 2019: 22.64<br/>NO2 2020: 17.86");
	L.marker([50.801038037975, -1.05485979297969], {icon: caution10sIcon}).addTo(mymap3).bindPopup("<b>Receptor 98</b><br/>NO2 2018: 22.51<br/>NO2 2019: 18.19<br/>NO2 2020: 17.1");
	L.marker([50.8008640937203, -1.05448014789891], {icon: caution10sIcon}).addTo(mymap3).bindPopup("<b>Receptor 99</b><br/>NO2 2018: 23.57<br/>NO2 2019: 20.29<br/>NO2 2020: 18.98");
	L.marker([50.8466435375357, -1.038555818249], {icon: caution10sIcon}).addTo(mymap3).bindPopup("<b>Receptor 100</b><br/>NO2 2018: 22.14<br/>NO2 2019: 19.94<br/>NO2 2020: 15.19");
	L.marker([50.8467439670331, -1.03983215993971], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 101</b><br/>NO2 2018: 28.17<br/>NO2 2019: 25<br/>NO2 2020: 22.21");
	L.marker([50.8473410666855, -1.08396719428306], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 102</b><br/>NO2 2018: 28.72<br/>NO2 2019: 23.71<br/>NO2 2020: 21.86");
	L.marker([50.8315334661147, -1.07048934006966], {icon: caution10sIcon}).addTo(mymap3).bindPopup("<b>Receptor 103</b><br/>NO2 2018: 24.73<br/>NO2 2019: 23.04<br/>NO2 2020: 18.16");
	L.marker([50.8176639413239, -1.07935298874652], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 108</b><br/>NO2 2018: 44.18<br/>NO2 2019: 32.46<br/>NO2 2020: 31.38");
	L.marker([50.8173481238961, -1.07921723984492], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 109</b><br/>NO2 2018: 35.76<br/>NO2 2019: 30.11<br/>NO2 2020: 26");
	L.marker([50.8176771877582, -1.07989217527166], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 110</b><br/>NO2 2018: 27.72<br/>NO2 2019: 22.15<br/>NO2 2020: 22.35");
	L.marker([50.8176339175447, -1.08010596499094], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 111</b><br/>NO2 2018: 28.73<br/>NO2 2019: 24.6<br/>NO2 2020: 21.53");
	L.marker([50.8006068263295, -1.09458849477768], {icon: caution40sIcon}).addTo(mymap3).bindPopup("<b>Receptor 117</b><br/>NO2 2018: 50.42<br/>NO2 2019: 48<br/>NO2 2020: 41.04");
	L.marker([50.8006013180672, -1.09387906190622], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 118</b><br/>NO2 2018: 50.38<br/>NO2 2019: 52.52<br/>NO2 2020: 38.76");
	L.marker([50.8027430445615, -1.09175150965837], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 119</b><br/>NO2 2018: 31.97<br/>NO2 2019: 30.67<br/>NO2 2020: 24.69");
	L.marker([50.802897228364, -1.09191881817834], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 120</b><br/>NO2 2018: 47.51<br/>NO2 2019: 46.9<br/>NO2 2020: 36.3");
	L.marker([50.814546329159, -1.07971235176826], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 121</b><br/>NO2 2018: 37.32<br/>NO2 2019: 38.55<br/>NO2 2020: 32.49");
	L.marker([50.8147185041692, -1.07987930890088], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 122</b><br/>NO2 2018: 37.68<br/>NO2 2019: 36.76<br/>NO2 2020: 30.77");
	L.marker([50.8551149036512, -1.11356450259112], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 124</b><br/>NO2 2018: 28.56<br/>NO2 2019: 26.07<br/>NO2 2020: 23.83");
	L.marker([50.8374424324373, -1.0694062638227], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 125</b><br/>NO2 2018: 39.58<br/>NO2 2019: 27.88<br/>NO2 2020: 30.05");
	L.marker([50.8432877310521, -1.09582178042474], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 126</b><br/>NO2 2018: 37.53<br/>NO2 2019: 38.66<br/>NO2 2020: 27.03");
	L.marker([50.8468995305685, -1.09887655472918], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 127</b><br/>NO2 2019: 28.44<br/>NO2 2020: 23.96");
	L.marker([50.8159286160975, -1.08280819594987], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 128</b><br/>NO2 2019: 23.39<br/>NO2 2020: 21.54");
	L.marker([50.8160813530719, -1.08279100865362], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 129</b><br/>NO2 2019: 23.18<br/>NO2 2020: 21.11");
	L.marker([50.816994659165, -1.07886929669808], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 130</b><br/>NO2 2019: 35.8<br/>NO2 2020: 36.15");
	L.marker([50.8136297799521, -1.07980132382357], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 131</b><br/>NO2 2019: 33.16<br/>NO2 2020: 27.28");
	L.marker([50.797014699288, -1.05999248053968], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 132</b><br/>NO2 2019: 39.36<br/>NO2 2020: 31.62");
	L.marker([50.8002012064733, -1.08067531424853], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 133</b><br/>NO2 2019: 35.73<br/>NO2 2020: 30.65");
	L.marker([50.8000981691621, -1.08015228222115], {icon: caution10sIcon}).addTo(mymap3).bindPopup("<b>Receptor 134</b><br/>NO2 2018: 25.41<br/>NO2 2019: 24.96<br/>NO2 2020: 19.51");
	L.marker([50.8469070814385, -1.08481382202988], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 135</b><br/>NO2 2019: 25.73<br/>NO2 2020: 23.62");
	L.marker([50.8466928518616, -1.0850168948273], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 136</b><br/>NO2 2019: 26.67<br/>NO2 2020: 25.35");
	L.marker([50.8468933514181, -1.09112093725077], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 137</b><br/>NO2 2019: 35.42<br/>NO2 2020: 30.67");
	L.marker([50.84667022083, -1.09133837291534], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 138</b><br/>NO2 2019: 38.31<br/>NO2 2020: 25.68");
	L.marker([50.8467294301951, -1.09316961088029], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 139</b><br/>NO2 2019: 33.74<br/>NO2 2020: 26.2");
	L.marker([50.8540819471685, -1.1090094732664], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 140</b><br/>NO2 2019: 24.64<br/>NO2 2020: 22.74");
	L.marker([50.7901963805934, -1.1008193536288], {icon: caution10sIcon}).addTo(mymap3).bindPopup("<b>Receptor 142</b><br/>NO2 2019: 17.67<br/>NO2 2020: 12.11");
	L.marker([50.8306194302824, -1.06866150631222], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 143</b><br/>NO2 2019: 33.35<br/>NO2 2020: 22.92");
	L.marker([50.8302977554607, -1.06892349013863], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 144</b><br/>NO2 2019: 40.81<br/>NO2 2020: 29.88");
	L.marker([50.8046763898576, -1.08942910856393], {icon: caution40sIcon}).addTo(mymap3).bindPopup("<b>Receptor 145</b><br/>NO2 2019: 53.91<br/>NO2 2020: 43.59");
	L.marker([50.8481010535583, -1.07429295325381], {icon: caution10sIcon}).addTo(mymap3).bindPopup("<b>Receptor 146</b><br/>NO2 2019: 26.69<br/>NO2 2020: 19.73");
	L.marker([50.8481866866099, -1.07375147353893], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 147</b><br/>NO2 2019: 26.17<br/>NO2 2020: 22.51");
	L.marker([50.8473226016891, -1.08276016210357], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 148</b><br/>NO2 2019: 24.19<br/>NO2 2020: 21.25");
	L.marker([50.8475389393521, -1.08282691625322], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 149</b><br/>NO2 2019: 33.93<br/>NO2 2020: 27.93");
	L.marker([50.847866522301, -1.08103064555208], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 150</b><br/>NO2 2019: 37.46<br/>NO2 2020: 30.57");
	L.marker([50.8476490650473, -1.0808218705717], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 151</b><br/>NO2 2019: 31.83<br/>NO2 2020: 24.81");
	L.marker([50.8477162687872, -1.07566421541896], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 152</b><br/>NO2 2019: 41.97<br/>NO2 2020: 35.79");
	L.marker([50.8479046143051, -1.0756036583302], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 153</b><br/>NO2 2019: 36.31<br/>NO2 2020: 27.23");
	L.marker([50.8473647306441, -1.07328488116023], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 154</b><br/>NO2 2019: 43.04<br/>NO2 2020: 32.1");
	L.marker([50.8475610602064, -1.07309631131846], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 155</b><br/>NO2 2019: 35.76<br/>NO2 2020: 27.42");
	L.marker([50.8465408274574, -1.09320169112421], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 156</b><br/>NO2 2019: 35.8<br/>NO2 2020: 25.5");
	L.marker([50.8058577424069, -1.08639735847552], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 157</b><br/>NO2 2019: 37.33<br/>NO2 2020: 28.03");
	L.marker([50.8256207509256, -1.0455319870321], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 158</b><br/>NO2 2019: 33.96<br/>NO2 2020: 27.8");
	L.marker([50.8256526602632, -1.04503437745177], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 159</b><br/>NO2 2019: 39.16<br/>NO2 2020: 34.23");
	L.marker([50.8248409932593, -1.04475278911066], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 160</b><br/>NO2 2019: 40.92<br/>NO2 2020: 31.69");
	L.marker([50.8247821104251, -1.04525092622341], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 161</b><br/>NO2 2019: 28.49<br/>NO2 2020: 25.09");
	L.marker([50.8334747804631, -1.04368223013922], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 162</b><br/>NO2 2019: 45.25<br/>NO2 2020: 32.68");
	L.marker([50.8335038583747, -1.04393726456111], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 163</b><br/>NO2 2019: 38.56<br/>NO2 2020: 30.45");
	L.marker([50.8479837918276, -1.0822215472365], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 164</b><br/>NO2 2019: 34.57<br/>NO2 2020: 26.7");
	L.marker([50.8482525063197, -1.08208839856842], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 165</b><br/>NO2 2019: 30.25<br/>NO2 2020: 26.97");
	L.marker([50.8252582423495, -1.0462918888462], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 166</b><br/>NO2 2019: 34.71<br/>NO2 2020: 29.47");
	L.marker([50.8046127625243, -1.0847469829457], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 167</b><br/>NO2 2019: 29.01<br/>NO2 2020: 27.63");
	L.marker([50.8304987296668, -1.06707352262638], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 168</b><br/>NO2 2019: 27.62<br/>NO2 2020: 22.53");
	L.marker([50.8306233626908, -1.06691483911842], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 169</b><br/>NO2 2019: 32.66<br/>NO2 2020: 32.35");
	L.marker([50.8053650907721, -1.08664823512983], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 170</b><br/>NO2 2019: 41.5<br/>NO2 2020: 30.72");
	L.marker([50.8053955088372, -1.08708760161011], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 171</b><br/>NO2 2019: 31.33<br/>NO2 2020: 23.07");
	L.marker([50.805321021513, -1.08791220215982], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 172</b><br/>NO2 2019: 38.77<br/>NO2 2020: 27.9");
	L.marker([50.7966272941374, -1.07678653891414], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 173</b><br/>NO2 2019: 41.88<br/>NO2 2020: 33.17");
	L.marker([50.8046018781031, -1.08450593150872], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 174</b><br/>NO2 2019: 31.3<br/>NO2 2020: 25.71");
	L.marker([50.8059558725718, -1.08629609761402], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 175</b><br/>NO2 2019: 37.55<br/>NO2 2020: 32.42");
	L.marker([50.8251053867532, -1.0462950174164], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 176</b><br/>NO2 2019: 29.41<br/>NO2 2020: 25.5");
	L.marker([50.8316902913382, -1.06873961079692], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 178</b><br/>NO2 2020: 29.98");
	L.marker([50.8479536979354, -1.08182440748625], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 179</b><br/>NO2 2020: 24.21");
	L.marker([50.8046941516512, -1.08940037923794], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 180</b><br/>NO2 2020: 37.55");
	L.marker([50.8078999427977, -1.08879872118966], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 181</b><br/>NO2 2020: 24.33");
	L.marker([50.8080269341623, -1.08893818105172], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 182</b><br/>NO2 2020: 30.04");
	L.marker([50.8081062884884, -1.08988757279368], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 183</b><br/>NO2 2020: 26.44");
	L.marker([50.8081075061444, -1.09004367283889], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 184</b><br/>NO2 2020: 24.23");
	L.marker([50.8369527891914, -1.06441712804793], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 185</b><br/>NO2 2020: 20.92");
	L.marker([50.8095622861105, -1.08747477074369], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 188</b><br/>NO2 2020: 27.4");
	L.marker([50.8097605462267, -1.08752768213291], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 189</b><br/>NO2 2020: 28.49");
	L.marker([50.80842223419, -1.08888791231902], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 190</b><br/>NO2 2020: 27.73");
	L.marker([50.808595845654, -1.08923936539269], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 191</b><br/>NO2 2020: 27.23");
	L.marker([50.8082226425562, -1.07722507718676], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 192</b><br/>NO2 2020: 27.61");
	L.marker([50.7959286440424, -1.0748705476338], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 193</b><br/>NO2 2020: 25.18");
	L.marker([50.8079771744824, -1.07688928045941], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 194</b><br/>NO2 2020: 26.66");
	L.marker([50.8077651923552, -1.07737601933175], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 213</b><br/>NO2 2020: 34.09");
	L.marker([50.8150683845669, -1.07977306754372], {icon: caution30sIcon}).addTo(mymap3).bindPopup("<b>Receptor 37</b><br/>NO2 2012: 43.9<br/>NO2 2013: 39.68<br/>NO2 2014: 45.68<br/>NO2 2015: 38.4<br/>NO2 2016: 41.21<br/>NO2 2017: 44.6<br/>NO2 2018: 40.57<br/>NO2 2019: 40.46<br/>NO2 2020: 32.34");
	L.marker([50.831406993287, -1.07266447423046], {icon: caution10sIcon}).addTo(mymap3).bindPopup("<b>Receptor 38</b><br/>NO2 2012: 21.1<br/>NO2 2013: 20.27<br/>NO2 2014: 22.17<br/>NO2 2015: 18.78<br/>NO2 2016: 20.05<br/>NO2 2017: 19.41<br/>NO2 2018: 18.68<br/>NO2 2019: 17.47<br/>NO2 2020: 17.37");
	L.marker([50.8169156542048, -1.06441957857916], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 39</b><br/>NO2 2012: 36.1<br/>NO2 2013: 33.52<br/>NO2 2014: 35.93<br/>NO2 2015: 32.81<br/>NO2 2016: 34.34<br/>NO2 2017: 35.22<br/>NO2 2018: 34<br/>NO2 2019: 31.12<br/>NO2 2020: 26.56");
	L.marker([50.8074035236013, -1.08741747904487], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 40</b><br/>NO2 2012: 36.9<br/>NO2 2013: 35.94<br/>NO2 2014: 36.51<br/>NO2 2015: 30.25<br/>NO2 2016: 35.48<br/>NO2 2017: 33.54<br/>NO2 2018: 33.95<br/>NO2 2019: 32.44<br/>NO2 2020: 26.55");
	L.marker([50.7983752168149, -1.09556816676155], {icon: caution20sIcon}).addTo(mymap3).bindPopup("<b>Receptor 53</b><br/>NO2 2018: 30.52<br/>NO2 2019: 27.8<br/>NO2 2020: 21.29");

</script>

</body>
</html>
