from ostn02python import OSGB
from ostn02python import OSTN02
import csv

def Sites():

	outDict = {}

	sites = csv.DictReader(open("manchester/nonautomatic.csv", "rt"))

	for li in sites:
		try:
			siteId = li["Diffusion \nTube ID"].replace("\n", "")
			xin, yin = float(li["X OS Grid \nRef \n(Easting)"]), float(li["Y OS Grid \nRef \n(Northing)"])
		
			(x,y,h) = OSTN02.OSGB36_to_ETRS89 (xin, yin)
			(gla, glo) = OSGB.grid_to_ll(x, y)

			outDict[siteId] = ((gla, glo))

		except Exception:
			pass

	sites = csv.DictReader(open("manchester/automatic.csv", "rt"))

	for li in sites:
		try:
			siteId = li["Site ID"].replace("\n", "")
			xin, yin = float(li["X OS Grid \nRef \n(Easting)"]), float(li["Y OS Grid \nRef \n(Northing)"])
		
			(x,y,h) = OSTN02.OSGB36_to_ETRS89 (xin, yin)
			(gla, glo) = OSGB.grid_to_ll(x, y)

			outDict[siteId] = ((gla, glo))

		except Exception:
			pass


	return outDict

def Measurements():
	
	rowsDict = {}

	data = csv.DictReader(open("manchester/nonautomatic.csv", "rt"))
	
	for li in data:
		#print (li)
		siteId = li["Diffusion \nTube ID"].replace("\n", "")

		years = []
		measurements = []
		for y in range(2016, 2021):
			reading = li[str(y)]
			if reading in ["", "-"]: continue
			years.append(y)
			measurements.append(float(reading))

		rowsDict[siteId] = ((years, measurements))

	data = csv.DictReader(open("manchester/automatic.csv", "rt"))
	
	for li in data:
		#print (li)
		siteId = li["Site ID"].replace("\n", "")

		years = []
		measurements = []
		for y in range(2016, 2021):
			reading = li[str(y)]
			if reading in ["", "-"]: continue
			years.append(y)
			measurements.append(float(reading))

		rowsDict[siteId] = ((years, measurements))

	return rowsDict

if __name__=="__main__":


	print (Sites())
