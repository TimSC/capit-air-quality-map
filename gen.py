import csv
import havant, portsmouth, locfareham, locsouthampton, locmanchester

def GenPage(measurementsDict, siteDict, fileOut, currentYear = 2022):

	fileOut.write("""<!DOCTYPE html>
<html>
<head>
	
	<title>NO2 measured levels """+str(currentYear)+"""</title>

	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<link rel="stylesheet" href="leaflet/leaflet.css"/>
	<script src="leaflet/leaflet.js"></script>
	
</head>
<body>

<div id="mapid" style="width: 100%; height: 500px;"></div>

<script>

	var mymap3 = L.map('mapid').setView([52.3717112, 0.4871126], 7);

	/*L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoidGltc2MiLCJhIjoiY2l0cmdpeXBoMDAwcDJ1cGtvdXNhN2FxbiJ9.9RHQRB-C3ZPZhRNRs5TYFA', {
		maxZoom: 18,
		attribution: 'Map data &copy; <a href="http://www.openstreetmap.org/about/">OpenStreetMap</a> contributors, ' +
			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		id: 'mapbox.streets'
	}).addTo(mymap3);*/
	
	L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoidGltc2MiLCJhIjoiY2l0cmdpeXBoMDAwcDJ1cGtvdXNhN2FxbiJ9.9RHQRB-C3ZPZhRNRs5TYFA', {
	attribution: '© <a href="https://www.mapbox.com/about/maps/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> <strong><a href="https://www.mapbox.com/map-feedback/" target="_blank">Improve this map</a></strong>',
	tileSize: 512,
	maxZoom: 18,
	zoomOffset: -1,
	id: 'mapbox/streets-v11',
	accessToken: 'pk.eyJ1IjoidGltc2MiLCJhIjoiY2l0cmdpeXBoMDAwcDJ1cGtvdXNhN2FxbiJ9.9RHQRB-C3ZPZhRNRs5TYFA'
	}).addTo(mymap3);

	var caution0sIcon = L.icon({
		iconUrl: 'caution-0s.png',

		iconSize:	 [13, 13], // size of the icon
		iconAnchor:   [6, 6], // point of the icon which will correspond to marker's location
		popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
	});

	var caution10sIcon = L.icon({
		iconUrl: 'caution-10s.png',

		iconSize:	 [26, 26], // size of the icon
		iconAnchor:   [13, 13], // point of the icon which will correspond to marker's location
		popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
	});

	var caution20sIcon = L.icon({
		iconUrl: 'caution-20s.png',

		iconSize:	 [38, 38], // size of the icon
		iconAnchor:   [19, 19], // point of the icon which will correspond to marker's location
		popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
	});

	var caution30sIcon = L.icon({
		iconUrl: 'caution-30s.png',

		iconSize:	 [51, 51], // size of the icon
		iconAnchor:   [25, 25], // point of the icon which will correspond to marker's location
		popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
	});

	var caution40sIcon = L.icon({
		iconUrl: 'caution-40s.png',

		iconSize:	 [64, 64], // size of the icon
		iconAnchor:   [32, 32], // point of the icon which will correspond to marker's location
		popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
	});

	var caution50sIcon = L.icon({
		iconUrl: 'caution-50s.png',

		iconSize:	 [77, 77], // size of the icon
		iconAnchor:   [38, 38], // point of the icon which will correspond to marker's location
		popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
	});

""")

	for siteId, (years, measurements) in measurementsDict.items():
		print (siteId, (years, measurements))

		if currentYear not in years: continue
		if siteId not in siteDict:
			print ("Missing site position {}".format(siteId))
			continue
		lat, lon = siteDict[siteId]
		current = measurements[years.index(currentYear)]
		cat = int(min(current // 10, 5) * 10)
		
		popupTxt = "<b>Receptor {}</b>".format(siteId)
		for y, m in zip(years, measurements):
			popupTxt += "<br/>NO2 {}: {}".format(y, m)

		fileOut.write("\tL.marker([{}, {}], {{icon: caution{}sIcon}}).addTo(mymap3).bindPopup(\"{}\");\n".format(lat, lon, cat, popupTxt))

	fileOut.write("""
</script>

</body>
</html>
""")
	fileOut.close()

def AddSitesWithPrefix(sites, prefix, sitesToAdd):

	for siteId, data in sitesToAdd.items():
		sites[prefix+siteId] = data

def AddMeasurementsWithPrefix(measurements, prefix, measurementsToAdd):

	for siteId, data in measurementsToAdd.items():
		measurements[prefix+siteId] = data

if __name__=="__main__":

	sites = {}
	AddSitesWithPrefix(sites, "hav-", havant.Sites())
	AddSitesWithPrefix(sites, "pom-", portsmouth.Sites())
	AddSitesWithPrefix(sites, "far-", locfareham.Sites())
	AddSitesWithPrefix(sites, "sot-", locsouthampton.Sites())
	AddSitesWithPrefix(sites, "man-", locmanchester.Sites())

	measurements = {}
	AddMeasurementsWithPrefix(measurements, "hav-", havant.Measurements())
	AddMeasurementsWithPrefix(measurements, "pom-", portsmouth.Measurements())
	AddMeasurementsWithPrefix(measurements, "far-", locfareham.Measurements())
	AddMeasurementsWithPrefix(measurements, "sot-", locsouthampton.Measurements())
	AddMeasurementsWithPrefix(measurements, "man-", locmanchester.Measurements())

	GenPage(measurements, sites, open("gen.html", "wt"))

