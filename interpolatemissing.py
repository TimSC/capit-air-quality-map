import csv
import datetime
import numpy as np
from matplotlib import pyplot as plt

if __name__=="__main__":

    data = csv.DictReader(open("portsmouth/measuredno2-provisional2024.csv", "rt"))
    dateDict = {}

    dataOut = csv.DictWriter(open("portsmouth/interpolated.csv", "wt"),
        fieldnames=data.fieldnames)
    dataOut.writeheader()

    data = list(data)

    for li in data:

        siteId = int(li['Network ID'])

        valRange = []
        for i in range(0,13):
            x = datetime.datetime(2023+(i//12), (i%12)+1, 1)
            dateStr = x.strftime("%B %Y")
            try:
                val = float(li[dateStr])
            except ValueError:
                val = None
            valRange.append(val)

        dateDict[siteId] = valRange

    # For sites with all data available, take the average at each month
    fullData = []
    for siteId, valRange in dateDict.items():
        
        numVals = sum([isinstance(val, float) for val in valRange])

        if numVals == 13:
            fullData.append(valRange)

    fullData = np.array(fullData)
    avFullData = np.mean(fullData, axis=0)

    #plt.plot(avFullData)
    #plt.show()

    # Interpolate missing data
    interpolatedData = {}
    for siteId, valRange in dateDict.items():
        
        numVals = sum([isinstance(val, float) for val in valRange])

        # Determine scaling
        measuredMonths = []
        matchingMonths = []
        for val, avVal in zip(valRange, avFullData):
            if val is not None:
                measuredMonths.append(val)
                matchingMonths.append(avVal)

        avMeasured = np.mean(measuredMonths)
        avMatching = np.mean(matchingMonths)
        scaling = avMeasured/avMatching
        
        interpolated = []
        for val, avVal in zip(valRange, avFullData):
            if val is None:
                interpolated.append(avVal*scaling)
            else: 
                interpolated.append(val)

        interpolatedData[siteId] = interpolated

    for li in data:

        siteId = int(li['Network ID'])

        for i in range(0,13):
            x = datetime.datetime(2023+(i//12), (i%12)+1, 1)
            dateStr = x.strftime("%B %Y")

            print (siteId, i)
            li[dateStr] = interpolatedData[siteId][i]

        dataOut.writerow(li)
    del dataOut

