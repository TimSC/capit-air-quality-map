from ostn02python import OSGB
from ostn02python import OSTN02
import csv

def Sites():

	sites = csv.DictReader(open("fareham/page.csv", "rt"))
	outDict = {}

	for li in sites:
		try:
			siteId = li['Diffusion Tube ID'].replace("\n", "")
			xin, yin = float(li['X OS Grid Ref (Easting)']), float(li['Y OS Grid Ref (Northing)'])
		
			(x,y,h) = OSTN02.OSGB36_to_ETRS89 (xin, yin)
			(gla, glo) = OSGB.grid_to_ll(x, y)

		except ValueError:
			pass
		outDict[siteId] = ((gla, glo))

	return outDict

def Measurements():
	
	data = csv.DictReader(open("fareham/page.csv", "rt"))
	rowsDict = {}
	
	for li in data:
		#print (li)
		siteId = li['Diffusion Tube ID'].replace("\n", "")

		years = []
		measurements = []
		for y in range(2016, 2021):
			reading = li[str(y)]
			if reading in ["", "-"]: continue
			years.append(y)
			measurements.append(float(reading))

		rowsDict[siteId] = ((years, measurements))

	return rowsDict

if __name__=="__main__":


	print (Sites())
