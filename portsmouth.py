from ostn02python import OSGB
from ostn02python import OSTN02
import csv

def Sites():

	sites = csv.DictReader(open("portsmouth/measuredno2-2023.csv", "rt"))
	outDict = {}

	for li in sites:
		try:
			siteId = li['Site ID']
			xin, yin = float(li['X']), float(li['Y'])
		
			(x,y,h) = OSTN02.OSGB36_to_ETRS89 (xin, yin)
			(gla, glo) = OSGB.grid_to_ll(x, y)

		except ValueError:
			(gla, glo) = None, None
		outDict[siteId] = ((gla, glo))

	return outDict

def Measurements():
	
	data = csv.DictReader(open("portsmouth/measuredno2-2023.csv", "rt"))
	rowsDict = {}
	
	for li in data:
		#print (li)
		siteId = li['Site ID']

		years = []
		measurements = []
		for y in range(2012, 2023):
			reading = li[str(y)]
			if reading == "": continue
			years.append(y)
			measurements.append(float(reading))

		rowsDict[siteId] = ((years, measurements))

	return rowsDict

if __name__=="__main__":


	print (Sites())
