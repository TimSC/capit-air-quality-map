import camelot
import os
import pandas as pd

def MergeHeaderLines(table):
	header = table.df.iloc[:2].to_numpy()
	for c in range(header.shape[1]):
		if header[1, c] != "":
			header[0, c] = header[1, c]

	table.df.iloc[:2] = header

	table.df.drop(index=table.df.index[1], 
			axis=0, 
			inplace=True)

if __name__=="__main__":

	if not os.path.exists("manchester/nonautomatic.csv"):

		print ("Manchester non-automatic")
		tables = camelot.read_pdf('/home/tim/Documents/airqual/manchester/AQAP_ASR_2021_WCAG.pdf', pages='147-177')

		tables2 = [tables[0].df]

		for i, table in enumerate(tables[1:]):

			table.df.drop(index=table.df.index[0], 
					axis=0, 
					inplace=True)
			tables2.append(table.df)

		result = pd.concat(tables2)

		result.to_csv("manchester/nonautomatic.csv", index=False, header=False)
	
	if not os.path.exists("manchester/automatic.csv"):

		print ("Manchester automatic")
		tables = camelot.read_pdf('/home/tim/Documents/airqual/manchester/AQAP_ASR_2021_WCAG.pdf', pages='145-146')

		tables2 = [tables[0].df]

		for i, table in enumerate(tables[1:]):

			table.df.drop(index=table.df.index[0], 
					axis=0, 
					inplace=True)
			tables2.append(table.df)

		result = pd.concat(tables2)

		result.to_csv("manchester/automatic.csv", index=False, header=False)

		tables2 = [tables[1].df]

		tables2.append(tables[2].df)

		result = pd.concat(tables2)

		result.to_csv("london/citymeasure4.csv", index=False, header=False)



